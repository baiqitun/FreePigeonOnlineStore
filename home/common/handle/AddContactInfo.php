<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/10
 * Time: 9:26
 */
require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Goods.php");
require_once ("../../../public/common/classes/Touch.php");

session_start();
postIsEmptyThenReturn();
$userId = $_SESSION['userId'];
$name = $_POST['name'];
$addr = $_POST['addr'];
$tel = $_POST['tel'];
$email = $_POST['email'];

textIsNullThenReturnWithMsg($name,'姓名不能为空！');
textIsNullThenReturnWithMsg($addr,'地址不能为空！');
textIsNullThenReturnWithMsg($tel,'电话号码不能为空！');
textIsNullThenReturnWithMsg($email,'电子邮箱不能为空！');

printArray($_SESSION);
printArray($_POST);

$touch = new Touch($pdo);
if (!$touch->addTouch($name,$addr,$tel,$email,intval($userId)))
{
    echo "<script>alert('添加失败！');history.go(-1);</script>";
    die();
}
gotoPageWithMsg('../../person/viewContactInfo.php','添加成功！');
?>