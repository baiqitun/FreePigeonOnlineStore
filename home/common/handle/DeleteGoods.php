<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/9
 * Time: 21:08
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Goods.php");

session_start();
$goodsId = $_GET['id'];

unset($_SESSION['goods'][$goodsId]);
echo "<script>alert('删除成功！');history.go(-1);</script>";
die();
?>