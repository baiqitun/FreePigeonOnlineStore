<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/11
 * Time: 7:02
 */


require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Touch.php");


session_start();
getIsEmptyByParamThenReturn('id');
sessionIsEmptyByParamThenReturn('userId');

$touchId = $_GET['id'];
$touch = new Touch($pdo);
if (!$touch->deleteTouchById(intval($touchId)))
{
    returnWithMsg('删除失败！');
}
gotoPageWithMsg('../../person/viewContactInfo.php','删除成功！');