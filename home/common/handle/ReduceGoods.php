<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/10
 * Time: 9:48
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Goods.php");

session_start();
$goodsId = $_GET['goodsId'];
$goods = new Goods($pdo);
$goodsRow = $goods->getGoodsById(intval($goodsId));

if (empty($_SESSION['goods'][$goodsId]))
{
    $_SESSION['goods'][$goodsId]=$goodsRow;
}

if ($_SESSION['goods'][$goodsId]['num'] > 1)
{
    $_SESSION['goods'][$goodsId]['num'] --;
}
echo "<script>history.go(-1);</script>";
?>