<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/10
 * Time: 10:30
 */
require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Goods.php");
require_once ("../../../public/common/classes/User.php");
require_once ("../../../public/common/classes/SoldGoods.php");
require_once ("../../../public/common/classes/Touch.php");
require_once ("../../../public/common/classes/OrderInfo.php");
require_once ("../../../public/common/classes/Status.php");

session_start();
sessionIsEmptyByParamThenGotoPage('userId','../../login.php');
postIsEmptyThenReturn();
if (sessionIsEmptyByParam('goods'))
{
    returnWithMsg('东西都没买就想结算？想得美');
}
textNotEqualThenReturnWithMsg($_POST['settle'],'yes','请进入购物车页面结算！');
textIsNullThenReturnWithMsg($_POST['touch'],'请选择一个联系方式！');

date_default_timezone_set('Asia/Shanghai');

$orderInfo = new OrderInfo($pdo);
$soldGood = new SoldGoods($pdo);


printArray($_SESSION);
foreach ($_SESSION['goods'] as $goodsRow)
{
    $soldGood->addSoldGoods($goodsRow['id'],$_SESSION['userId']);
}
if (!$orderInfo->addOrderInfo($_SESSION['userId'],1,intval($_POST['touch']),md5(microtime(true))))
{
    returnWithMsg('结算失败！');
}
unset($_SESSION['goods']);
gotoPageWithMsg('../../index.php','结算成功！');
?>