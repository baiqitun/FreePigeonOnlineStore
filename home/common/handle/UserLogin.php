<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 20:37
 */
require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/User.php");

session_start();

$username = $_POST['username'];
$password = $_POST['password'];

textIsNullThenReturnWithMsg($username,'用户名不能为空！');
textIsNullThenReturnWithMsg($password,'密码不能为空！');

$user = new User($pdo);
$userRow = $user->getUserByUsername($username);

objNotEqualThenReturnWithMsg($password,$userRow['password'],'用户名或密码错误！');

$_SESSION['userId']  = $userRow['id'];

gotoPageWithMsg("../../index.php",'登录成功！');