<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/11
 * Time: 7:19
 */
require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Touch.php");

session_start();
sessionIsEmptyByParamThenReturn('userId');
postIsEmptyThenReturn();

$id = $_POST['id'];
$name = $_POST['name'];
$tel = $_POST['tel'];
$addr = $_POST['addr'];
$email = $_POST['email'];

printArray($_POST);

$touch = new Touch($pdo);
if (!$touch->modifyTouchById($id,$name,$addr,$tel,$email,$_SESSION['userId']))
{
    echo "<script>alert('修改失败：原因可能是您没有修改任何数据！');history.go(-1);</script>";
    die();
}
gotoPageWithMsg('../../person/viewContactInfo.php','修改成功！')
?>