<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/10
 * Time: 18:06
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Goods.php");
require_once ("../../../public/common/classes/Touch.php");
require_once ("../../../public/common/classes/User.php");
require_once ("../../../public/common/classes/Comment.php");

session_start();
sessionIsEmptyByParamThenReturn('userId');
postIsEmptyThenReturn();
$commentText=$_POST['comment'];
$goodId=$_POST['goodId'];

$comment = new Comment($pdo);
if (!$comment->addComment($_SESSION['userId'],$commentText,$goodId))
{
    echo "<script>alert('评论失败！');history.go(-1);</script>";
    die();
}
echo "<script>alert('评论成功！');location='../../shop.php?id={$goodId}';</script>";
die();