<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/9
 * Time: 19:47
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Goods.php");

session_start();
$goodsId = $_GET['goodsId'];
$goods = new Goods($pdo);
$goodsRow = $goods->getGoodsById(intval($goodsId));

if (empty($_SESSION['goods'][$goodsId]))
{
    $_SESSION['goods'][$goodsId]=$goodsRow;
}
$_SESSION['goods'][$goodsId]['num'] ++;
if ($_GET['act'] == "shop")
{
    echo "<script>alert('加入购物车成功！');</script>";
}
echo "<script>history.go(-1);</script>";
?>