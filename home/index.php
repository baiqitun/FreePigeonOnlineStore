<?php
require_once ("../public/common/DbConfig.php");
require_once ("../public/common/db_connect.php");
require_once ("../public/common/public_include.php");
require_once ("../public/common/classes/GoodsClass.php");
require_once ("../public/common/classes/Advert.php");
require_once ("../public/common/classes/Brand.php");
require_once ("../public/common/classes/Goods.php");
require_once ("../public/common/classes/User.php");
session_start();

?>
<!DOCTYPE html>
<html lang="zh-CN">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>飞鸽网上商城欢迎你！</title>

	<link href="../public/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/index.css" rel="stylesheet">
	<link rel="icon" href="../favicon.ico" type="image/x-icon">
</head>

<body data-spy="scroll" data-target="#menu-bar" data-offset="200">
	<div class="container-fluid">
        <div class="row">
            <nav class="navbar navbar-default navbar-inverse navbar-fixed-top" style="border: 2px solid black;">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">飞鸽网上商城</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <form class="navbar-form navbar-left" action="search.php" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="输入商品名" name="search">
                            </div>
                            <button type="submit" class="btn btn-default">搜索</button>
                        </form>
                        <ul class="nav navbar-nav navbar-right">
                            <?php
                            $user = new User($pdo);
                            if (sessionIsEmptyByParam('userId'))
                            {
                                ?>
                                <li><a href="login.php">请登录</a></li>

                                <?php
                            }
                            else
                            {
                                $userRow = $user->getUserById(intval($_SESSION['userId']));
                                ?>
                                <li><a href="#"><?php echo "亲爱的{$userRow['username']},欢迎回来！";?></a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">个人中心<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="person.php">我的中心</a>
                                        </li>
                                        <li>
                                            <a href="cart/index.php">
                                                购物车
                                                <?php
                                                if (!empty($_SESSION['goods']))
                                                {
                                                $goodsCount = 0;
                                                foreach ($_SESSION['goods'] as $goodsRow)
                                                {
                                                    $goodsCount += $goodsRow['num'];
                                                }
                                                ?>
                                                <span class="badge">
                                                    <?php echo "$goodsCount";?>
                                                </span></a>
                                            <?
                                            }
                                            ?>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="common/handle/UserLogout.php?logout=yes">注销</a></li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
        <div class="container">
            <div class="row" style="margin-top: 10px;">
                <?php
                $advert = new Advert($pdo);
                $advRows = $advert->getAllUsingAdvert();
                if ($advRows != null)
                {
                    ?>
                    <div id="carousel-example-generic" class="carousel slide col-lg-12 col-md-12 hidden-xs hidden-sm" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php
                            $advIndex = 0;
                            foreach ($advRows as $advRow)
                            {
                                ?>
                                <li data-target="#carousel-example-generic" data-slide-to="<?php
                                echo $advIndex;
                                ?>"
                                    class="<?php
                                    if ($advIndex == 0)
                                    {
                                        echo "active";
                                    }
                                    ?>">

                                </li>
                                <?php
                                $advIndex ++;
                            }
                            ?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner col-lg-12 col-md-12" role="listbox">
                            <?php
                            $advIndex = 0;
                            if ($advRows != null)
                            {
                                foreach ($advRows as $advRow)
                                {
                                    ?>
                                    <div class="item
                                    <?php
                                    if ($advIndex == 0)
                                    {
                                        echo "active";
                                    }
                                    ?>">
                                        <a href="shop.php?id=<?php echo "{$advRow['shop_id']}";?>">
                                            <img src=
                                                 "../public/img/AdvertImg/<?php
                                                 echo $advRow['img'];
                                                 ?>" alt="" class="img-responsive center-block">
                                        </a>
                                        <a href="shop.php?id=<?php echo "{$advRow['shop_id']}";?>">
                                            <div class="carousel-caption">
                                                <h3>
                                                    <?php
                                                    echo $advRow['advTitle'];
                                                    ?>
                                                </h3>
                                                <p>
                                                    <?php
                                                    echo $advRow['advContent'];
                                                    ?>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                    $advIndex ++;
                                }
                            }
                            ?>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        </a>
                    </div>
                    <?php
                }
                else
                {
                    ?>
                    <img class="col-lg-12 col-md-12 col-sm-12" src="../public/img/login_bg_1.jpg" alt="" style="border: 2px solid black; height: 300px;">
                    <?php
                }
                ?>

            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-lg-2 col-md-2 hidden-sm hidden-xs hidden-md" id="menu-bar">
                    <ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="150" >
                        <?php
                            $goodsClass = new GoodsClass($pdo);
                            $goodsClassRows = null;
                            $goodsClassRows = $goodsClass->getAllGoodsClass();
                            $classCount = 1;
                            if ($goodsClass != null)
                            {
                                foreach ($goodsClassRows as $row)
                                {
                                    ?>
                                    <li <?php if($classCount == 1)echo "class=\"active\"";?>><a href="#goodsClass-<?php echo "{$classCount}";?>"><?php echo "{$row['name']}"?></a></li>
                                    <?php
                                    $classCount ++;
                                }
                            }
                        ?>
                    </ul>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="margin-top: 20px;">

                    <?php
                    $goods = new Goods($pdo);
                    $brand = new Brand($pdo);
                    $goodsRows = $goods->getAllGoods();

                    $classCount = 1;
                    if ($goodsClassRows != null)
                    {
                        foreach ($goodsClassRows as $row)
                        {
                            ?>
                            <div class="row" id="goodsClass-<?php echo "{$classCount}";?>">
                                <div class="row-header"><h3><?php echo "{$row['name']}";?></h3><a href="class.php?classId=<?php echo "{$row['id']}";?>">more</a></div>
                                <div class="row-content">
                                    <?php
                                    $goodsRows = $goods->getAllGoodsByClassId($row['id']);

                                    //printArray($goodsRows);
                                    if ($goodsRows != null)
                                    {
                                        foreach ($goodsRows as $goodsRow)
                                        {
                                            ?>
                                            <div class="col-ls-3 col-md-3">
                                                <a href="shop.php?id=<?php echo $goodsRow['id'];?>" class="thumbnail">
                                                    <img src="../public/img/GoodsImg/small/<?php echo "{$goodsRow['img']}"?>" alt="...">
                                                </a>
                                                <span class="text-center center-block"><?php echo $goodsRow['name']?></span>
                                                <span class="text-center center-block">
                                                    <?php
                                                    $brandRow = $brand->getBrandById($goodsRow['brand_id']);
                                                    echo $brandRow['name'];
                                                    ?>
                                                </span>
                                                <span class="text-center center-block"><span style="color: orange; font-size: 16px;font-weight: bolder;">￥</span><span><?php echo $goodsRow['price']?></span></span>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <?
                            $classCount ++;
                            }
                        }
                    ?>
                </div>
            </div>
            <div class="outside-bar text-center">
                <div class="side-bar-item">
                    <a href="#">
                        <span class="glyphicon glyphicon-triangle-top" id="goto-top"></span>
                    </a>
                </div>
            </div>
        </div>
    <footer>
    </footer>
	<script src="../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
        <script src="../public/js/bootstrap.js"></script>
        <script src="../public/js/holder.js"></script>
</body>

</html>


<!--<nav class="navbar navbar-default navbar-fixed-top navbar-inverse">-->
<!--    <div class="container">-->
<!--        <div class="navbar-header">-->
<!--            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"-->
<!--                    aria-expanded="false">-->
<!--                <span class="sr-only">Toggle navigation</span>-->
<!--                <span class="icon-bar"></span>-->
<!--                <span class="icon-bar"></span>-->
<!--                <span class="icon-bar"></span>-->
<!--            </button>-->
<!--            <a class="navbar-brand" href="#">飞鸽网上商城</a>-->
<!--        </div>-->
<!--        <form class="navbar-form navbar-left">-->
<!--            <div class="form-group">-->
<!--                <input type="text" class="form-control" placeholder="输入书名、作者">-->
<!--            </div>-->
<!--            <button type="submit" class="btn btn-default">搜索</button>-->
<!--        </form>-->
<!--        <ul class="nav navbar-nav navbar-right">-->
<!--            <li>-->
<!--                <a href="" style="">请登录</a>-->
<!--            </li>-->
<!--            <li class="dropdown">-->
<!--                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="display: --><?php //echo $displayBtn?><!--">个人中心-->
<!--                    <span class="caret"></span>-->
<!--                </a>-->
<!--                <ul class="dropdown-menu ">-->
<!--                    <li>-->
<!--                        <a href="#">购物车</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#">Another action</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#">Something else here</a>-->
<!--                    </li>-->
<!--                    <li role="separator" class="divider"></li>-->
<!--                    <li>-->
<!--                        <a href="">注销</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
<!--        </ul>-->
<!--    </div>-->
<!--</nav>-->

