<?php
require_once("../public/common/classes/Captcha.php");
?>
<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>飞鸽网上书城-用户注册</title>

    <link href="../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/register.css" rel="stylesheet">
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
</head>

<body>
<header>
    <div class="container">
        <div class="row">
            <p class="col-lg-12 col-md-12 col-sm-12">飞鸽网上书城
                <img id="main-logo" class="pull-left" src="../public/img/pigeon.png">
            </p>
        </div>
    </div>
</header>
<div class="container-fluid">
    <div class="container" id="main-content">
        <form class="form-horizontal" action="common/handle/UserRegister.php" method="post">
            <h3 class="text-center">用户注册</h3>
            <div class="form-group">
                <div class="col-sm-12">

                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="email" class="form-control" id="input-username" placeholder="用户名" name="username">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="password" class="form-control" id="input-password" placeholder="密码" name="password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="password" class="form-control" id="input-confirmPassword" placeholder="确认密码" name="confirmPassword">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="input-verificationCode" placeholder="验证码" name="verificationCode">
                </div>
            </div>
            <img src="../public/common/CreateVerificationCode.php" class="center-block" style="border: 1px solid #ccc; border-radius: 4px;">
            <div class="form-group">
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-block">注册</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../public/js/bootstrap.min.js"></script>
</body>

</html>
