<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 11:07
 */
require_once ("../public/common/DbConfig.php");
require_once ("../public/common/db_connect.php");
require_once ("../public/common/public_include.php");
require_once ("../public/common/classes/GoodsClass.php");
require_once ("../public/common/classes/Brand.php");
require_once ("../public/common/classes/Goods.php");
require_once ("../public/common/classes/User.php");

session_start();
getIsEmptyByParamThenReturn('classId');
$goodClass = new GoodsClass($pdo);
?>


<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>商城分类</title>

    <link href="../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/class.css" rel="stylesheet">
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
</head>

<body>
<header>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-default navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                                aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php">飞鸽网上商城</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <?php
                            $user = new User($pdo);
                            $userRow = $user->getUserById($_SESSION['userId']);
                            if ($userRow == null)
                            {
                                ?>
                                <li><a href="login.php">请登录</a></li>
                                <?php
                            }
                            else
                            {
                                ?>
                                <li><a href="">亲爱的<?php echo "{$userRow['username']}欢迎回来！";?></a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="display: <?php echo $displayBtn?>">个人中心
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu ">
                                        <li>
                                            <a href="person.php">我的中心</a>
                                        </li>
                                        <li>
                                            <a href="cart/index.php">
                                                购物车
                                                <?php
                                                if (!empty($_SESSION['goods']))
                                                {
                                                $goodsCount = 0;
                                                foreach ($_SESSION['goods'] as $goodsRow)
                                                {
                                                    $goodsCount += $goodsRow['num'];
                                                }
                                                ?>
                                                <span class="badge">
                                                    <?php echo "$goodsCount";?>
                                                </span></a>
                                            <?
                                            }
                                            ?>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <a href="common/handle/UserLogout.php?logout=yes">注销</a>
                                        </li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>

                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="row">
            <p class="col-lg-12 col-md-12 col-sm-12">飞鸽网上商城
                <img id="main-logo" class="pull-left" src="../public/img/pigeon.png">
            </p>
        </div>
    </div>
</header>
<div class="container" id="main-content">
    <div class="row nav">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="background: #222">
                <li><a href="index.php">首页</a></li>
                <li class="active">
                    <?php
                    $goodClassRow = $goodClass->getGoodsClassById(intval($_GET['classId']));
                    echo $goodClassRow['name'];
                    ?>
                </li>
<!--                <a href="brand.php?classId=--><?php //echo $goodClassRow['id']?><!--" class="pull-right">品牌</a>-->
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="row-content">
            <?php
            $goods = new Goods($pdo);
            $brand = new Brand($pdo);
            $goodsRows = $goods->getAllGoodsByClassId($_GET['classId']);
            //printArray($_GET);
            if ($goodsRows != null)
            {
                foreach ($goodsRows as $goodsRow)
                {
                    ?>
                    <div class="col-ls-3 col-md-3">
                        <a href="shop.php?id=<?php echo $goodsRow['id'];?>" class="thumbnail">
                            <img src="../public/img/GoodsImg/small/<?php echo "{$goodsRow['img']}"?>" alt="...">
                        </a>
                        <span class="text-center center-block"><?php echo $goodsRow['name']?></span>
                        <span class="text-center center-block">
                                                    <?php
                                                    $brandRow = $brand->getBrandById($goodsRow['brand_id']);
                                                    echo $brandRow['name'];
                                                    ?>
                                                </span>
                        <span class="text-center center-block"><span style="color: orange; font-size: 16px;font-weight: bolder;">￥</span><span><?php echo $goodsRow['price']?></span></span>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
<script src="../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../public/js/bootstrap.min.js"></script>
</body>

</html>
