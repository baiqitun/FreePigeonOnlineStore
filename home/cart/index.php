<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 14:25
 */
require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Touch.php");
require_once ("../../public/common/classes/User.php");

session_start();
sessionIsEmptyByParamThenGotoPage('userId','../index.php');
?>
<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>我的购物车</title>

    <link href="../../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/cart.css" rel="stylesheet">
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
</head>

<body>
<header>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-default navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                                aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../index.php">飞鸽网上商城</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <?php
                            $user = new User($pdo);
                            $userRow = $user->getUserById($_SESSION['userId']);
                            if ($userRow == null)
                            {
                                ?>
                                <li><a href="../login.php">请登录</a></li>
                                <?php
                            }
                            else
                            {
                                ?>
                                <li><a href="../login.php">亲爱的<?php echo "{$userRow['username']}欢迎回来！";?></a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="display: <?php echo $displayBtn?>">个人中心
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu ">
                                        <li>
                                            <a href="../person.php">我的中心</a>
                                        </li>
                                        <li>
                                            <a href="../cart/index.php">
                                                购物车
                                                <?php
                                                if (!empty($_SESSION['goods']))
                                                {
                                                $goodsCount = 0;
                                                foreach ($_SESSION['goods'] as $goodsRow)
                                                {
                                                    $goodsCount += $goodsRow['num'];
                                                }
                                                ?>
                                                <span class="badge">
                                                    <?php echo "$goodsCount";?>
                                                </span></a>
                                            <?
                                            }
                                            ?>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <a href="../common/handle/UserLogout.php?logout=yes">注销</a>
                                        </li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>

                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="row">
            <p class="col-lg-12 col-md-12 col-sm-12">飞鸽网上商城
                <img id="main-logo" class="pull-left" src="../../public/img/pigeon.png">
            </p>
        </div>
    </div>
</header>
<div class="container" id="main-content">
    <div class="row nav">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="background: #222;">
                <li><a href="../index.php">首页</a></li>
                <li class="active">我的购物车</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="table-wapper">
            <table class="table table-bordered .table-striped">
                <?php
                if ($_SESSION['goods'] != null)
                {
                    ?>
                    <tr>
                        <td>商品名称</td>
                        <td class="hidden-xs">图片</td>
                        <td>价格</td>
                        <td>数量</td>
                        <td>合计</td>
                        <td>操作</td>
                    </tr>
                    <?php
                    $money = 0;
                    foreach ($_SESSION['goods'] as $goodsRow)
                    {
                        ?>
                        <tr>
                            <td><?php echo $goodsRow['name'];?></td>
                            <td class="hidden-xs"><img src="../../public/img/GoodsImg/small/<?php echo "{$goodsRow['img']}";?>"></td>
                            <td></td>
                            <td>
                                <span>
                                    <a class="span-btn btn btn-default btn-sm" href="../common/handle/ReduceGoods.php?goodsId=<?php echo "{$goodsRow['id']}"?>">-</a>
                                    <span class="goodsNumber"><?php echo "{$goodsRow['num']}"?></span>
                                    <a class="span-btn btn btn-default btn-sm" href="../common/handle/AddGoods.php?act=care&goodsId=<?php echo "{$goodsRow['id']}"?>">+</a>
                                </span>
                            </td>
                            <td><?php echo $goodsRow['num']*$goodsRow['price'];?><span style="color: orange;">￥</span></td>
                            <td><a href="../common/handle/DeleteGoods.php?id=<?php echo "{$goodsRow['id']}";?>">删除</a></td>
                        </tr>
                        <?php
                        $money = $goodsRow['num']*$goodsRow['price'] + $money;
                    }
                    ?>
                    <tr><td class="cart-money-title text-right" colspan="6"><span><span>共计:</span><span><?php echo "$money";?></span><span style="color:orange;">￥</span></span></td></tr>
                    <?php
                }
                else
                {
                    echo "购物车空空如也，什么也没有！";
                }
                ?>
            </table>
        </div>
    </div>

    <?php
    if (!empty($_SESSION['goods']))
    {
        ?>
        <div class="row nav">
            <div class="col-lg-12">
                <ol class="breadcrumb" style="background: #333;">
                    <li><a href="../index.php">首页</a></li>
                    <li class="active">我的联系方式</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="table-wapper">
                <?php
                 $touch = new Touch($pdo);
                 $touchRows = $touch->getTouchByUserId($_SESSION['userId']);
                 if($touchRows != null)
                 {
                     ?>
                     <form action="../common/handle/Settle.php" method="post">
                         <input type="hidden" name="settle" value="yes">
                         <table class="table table-bordered .table-striped">
                             <tr>
                                 <td>选择</td>
                                 <td class="hidden-xs">姓名</td>
                                 <td>地址</td>
                                 <td>电话</td>
                                 <td class="hidden-xs">邮箱</td>
                             </tr>
                             <?php
                             $touchCount = 1;
                             foreach ($touchRows as $touchRow)
                             {
                                 ?>
                                 <tr>
                                     <td><input type="radio" name="touch" <?php if ($touchCount == 1)echo "checked";?> value="<?php echo "{$touchRow['id']}";?>"></td>
                                     <td class="hidden-xs"><?php echo "{$touchRow['name']}"?></td>
                                     <td><?php echo "{$touchRow['addr']}"?></td>
                                     <td><?php echo "{$touchRow['tel']}"?></td>
                                     <td class="hidden-xs"><?php echo "{$touchRow['email']}"?></td>
                                 </tr>
                                 <?php
                                 $touchCount ++;
                             }
                             ?>
                             <tr><td colspan="5"><button " class="btn btn-default btn-lg">结算购物车</button></td></tr>
                         </table>
                     </form>
                     <?php
                 }
                 else
                 {
                     echo "请先去<b>【个人中心】</b>-><b>【我的中心】</b>添加完联系方式后才可结算购物车!";
                 }
                ?>
            </div>
        </div>
        <?php
    }
    ?>

</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.min.js"></script>
</body>

</html>


