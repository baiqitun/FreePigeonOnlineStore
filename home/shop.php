<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 11:07
 */
require_once ("../public/common/DbConfig.php");
require_once ("../public/common/db_connect.php");
require_once ("../public/common/public_include.php");
require_once ("../public/common/classes/GoodsClass.php");
require_once ("../public/common/classes/Advert.php");
require_once ("../public/common/classes/Brand.php");
require_once ("../public/common/classes/Goods.php");
require_once ("../public/common/classes/User.php");
require_once ("../public/common/classes/SoldGoods.php");
require_once ("../public/common/classes/Comment.php");

session_start();
?>


<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>商品详情</title>

    <link href="../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/shop.css" rel="stylesheet">
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
</head>

<body>
<header>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-default navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                                aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">飞鸽网上商城</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <?php
                            $user = new User($pdo);
                            $userRow = $user->getUserById($_SESSION['userId']);
                            if ($userRow == null)
                            {
                                ?>
                                <li><a href="login.php">请登录</a></li>
                                <?php
                            }
                            else
                            {
                                ?>
                                <li><a href="">亲爱的<?php echo "{$userRow['username']}欢迎回来！";?></a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="display: <?php echo $displayBtn?>">个人中心
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu ">
                                        <li>
                                            <a href="person.php">我的中心</a>
                                        </li>
                                        <li>
                                            <a href="cart/index.php">
                                                购物车
                                                <?php
                                                if (!empty($_SESSION['goods']))
                                                {
                                                    $goodsCount = 0;
                                                    foreach ($_SESSION['goods'] as $goodsRow)
                                                    {
                                                        $goodsCount += $goodsRow['num'];
                                                    }
                                                    ?>
                                                <span class="badge">
                                                    <?php echo "$goodsCount";?>
                                                </span></a>
                                                    <?
                                                }
                                                ?>
                                        </li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            <a href="common/handle/UserLogout.php?logout=yes">注销</a>
                                        </li>
                                    </ul>
                                </li>
                                <?php
                            }
                            ?>

                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="row">
            <p class="col-lg-12 col-md-12 col-sm-12">飞鸽网上商城
                <img id="main-logo" class="pull-left" src="../public/img/pigeon.png">
            </p>
        </div>
    </div>
</header>
<div class="container" id="main-content">
    <div class="row nav">
        <?php
        $goods = new Goods($pdo);
        $goodsId = $_GET['id'];
        $goodsRow = $goods->getGoodsById($goodsId);
        ?>
        <div class="col-lg-12">
            <ol class="breadcrumb" style="background: #222">
                <li><a href="index.php">首页</a></li>
                <li class="active"><?php echo $goodsRow['name']?></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="row-content">
            <div class="row-content-img col-lg-4 col-md-4 col-lg-offset-4 col-sm-offset-4">
                <img src="../public/img/GoodsImg/real/<?php echo $goodsRow['img'];?>" class="col-lg-12 col-md-12" alt="">
            </div>
        </div>
    </div>
    <div class="row row-info">
        <div class="row-content-info text-center col-lg-12 col-md-12">
            <p><span><?php echo $goodsRow['price'];?></span><span style="color: orange;">￥</span></p>
        </div>
    </div>
    <?php
    if (!empty($_SESSION['userId']))
    {
        ?>
        <div class="row text-center">
            <a class="btn btn-default btn-group-lg" href="common/handle/AddGoods.php?act=shop&goodsId=<?php echo $goodsRow['id']?>">加入购物车</a>
        </div>
        <?php
    }
    else
    {
        ?>
        <div class="row text-center">
            <span>登录后才可以购物哦！</span>
        </div>
        <?php
    }
    ?>

    <div class="row row-comment">
        <div class="row-comment-content col-lg-12 col-md-12">
            <div class="comment-title col-lg-12 col-md-12"><h3>评论</h3></div>
            <?php
            $soldGoods = new SoldGoods($pdo);
            if ($soldGoods->isFindRecordByShopIdAndUserId($goodsId,$_SESSION['userId']))
            {
                ?>
                <div class="row">
                    <form action="common/handle/AddComment.php" method="post">
                        <label for="input-comment"></label>
                        <input name="goodId" value="<?php echo $goodsId?>" type="hidden">
                        <textarea name="comment" id="input-comment" class="col-lg-12 col-md-12" style="height: 150px;">
                </textarea>
                        <button type="submit">提交我的评论</button>
                    </form>
                </div>
                <?php
            }
            ?>
            <?php
            $comment = new Comment($pdo);
            $commentRows = $comment->getCommentByShopId($goodsId);
            if ($commentRows != null)
            {
                foreach ($commentRows as $commentRow)
                {
                    ?>
                    <div class="comment col-lg-12 col-md-12">
                        <div class="comment-info">
                            <span class="comment-user">用户
                                <?php
                                $user = new User($pdo);
                                $userRow = $user->getUserById($commentRow['user_id']);
                                echo "{$userRow['username']}";
                                ?>评论：
                            </span>
                        </div>
                        <div class="comment-content-text">
                            <?php echo "{$commentRow['content']}";?>
                        </div>
                        <div class="text-right">
                            <span class="comment-time">评论时间:
                                <?php echo "{$commentRow['time']}"?>
                            </span>
                        </div>
                    </div>
                    <?php
                }
            }
            else
            {
                ?>
                <div class="comment col-lg-12 col-md-12">
                    <?php echo "评论区什么也没有咩(^ = ^)"?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<script src="../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../public/js/bootstrap.min.js"></script>
</body>

</html>


