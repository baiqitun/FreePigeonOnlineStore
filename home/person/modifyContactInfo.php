<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/11
 * Time: 7:06
 */
require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Touch.php");
require_once ("../../public/common/classes/User.php");

session_start();
sessionIsEmptyByParamThenGotoPage('userId','../index.php');
getIsEmptyByParamThenReturn('id');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>修改联系方式页面</title>

    <link href="../../public/css/bootstrap.css" rel="stylesheet">
    <link href="../css/addContackInfo.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid" id="main-body">
    <div class="row text-center" id="main-body">
        <div class="panel panel-default col-md-3 col-lg-3 " id="add-user-panel">
            <!-- Default panel contents -->
            <div class="panel-heading">修改联系方式</div>
            <div class="panel-body">
                <form class="form-horizontal" action="../common/handle/ModifyContactInfo.php?userId=<?php echo $_SESSION['userId'];?>" method="post">
                    <?php
                    $touch = new Touch($pdo);
                    $touchRow = $touch->getTouchById($_GET['id']);
                    //printArray($touchRow);
                    ?>
                    <input type="hidden" name="id" value="<?php echo "{$touchRow['id']}"?>">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="input-username" placeholder="姓名" name="name" value="<?php echo "{$touchRow['addr']}";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12" >
                            <input type="text" class="form-control" id="input-email" placeholder="地址" name="addr" value="<?php echo "{$touchRow['addr']}";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12" >
                            <input type="text" class="form-control" id="input-password" placeholder="电话" name="tel" value="<?php echo "{$touchRow['tel']}";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12" >
                            <input type="text" class="form-control" id="input-affirm-password" placeholder="邮箱" name="email" value="<?php echo "{$touchRow['email']}";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class=" col-lg-12  col-md-12">
                            <button type="submit" class="btn btn-default">确认修改</button>
                            <a type="reset" class="btn btn-default" href="viewContactInfo.php">取消操作</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>