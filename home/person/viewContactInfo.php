<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 16:00
 */
require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Touch.php");
require_once ("../../public/common/classes/User.php");

session_start();
sessionIsEmptyByParamThenGotoPage('userId','../index.php');
?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>查看联系方式</title>
    <link href="../../public/css/bootstrap.css" rel="stylesheet">
</head>

<body style="margin-top: 10px;">
<div class="container-fluid" id="main-body">
    <div class="row" id="main-body">
        <?php
        $touch = new Touch($pdo);
        $touchRows = $touch->getTouchByUserId($_SESSION['userId']);
        if ($touchRows != null)
        {
            ?>
            <table class="table table-bordered text-center" style="overflow: scroll">
                <tr>
                    <td>姓名</td>
                    <td>地址</td>
                    <td>电话</td>
                    <td>邮箱</td>
                    <td>操作</td>
                </tr>
                <?php
                foreach ($touchRows as $touchRow)
                {
                    ?>
                    <tr>
                        <td><?php echo "{$touchRow['name']}"?></td>
                        <td><?php echo "{$touchRow['addr']}"?></td>
                        <td><?php echo "{$touchRow['tel']}"?></td>
                        <td><?php echo "{$touchRow['email']}"?></td>
                        <td><a class="pull-left" href="modifyContactInfo.php?id=<?php echo "{$touchRow['id']}"?>">修改</a><a class="pull-right" href="../common/handle/DeleteContactInfo.php?id=<?php echo "{$touchRow['id']}";?>">删除</a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
        }
        else
        {
            echo "<h2>请先去添加联系方式！</h2>";
        }
        ?>

    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>


