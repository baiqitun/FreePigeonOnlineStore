<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 16:01
 */
require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Touch.php");
require_once ("../../public/common/classes/User.php");
require_once ("../../public/common/classes/OrderInfo.php");
require_once ("../../public/common/classes/Status.php");

session_start();
sessionIsEmptyByParamThenGotoPage('userId','../index.php');
?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>查看联系方式</title>
    <link href="../../public/css/bootstrap.css" rel="stylesheet">
</head>

<body style="margin-top: 10px;">
<div class="container-fluid" id="main-body">
    <div class="row" id="main-body">
        <?php
        $orderInfo = new OrderInfo($pdo);
        $orderInfoRows = $orderInfo->getOrderInfoByUserId($_SESSION['userId']);
        if ($orderInfoRows != null)
        {

            ?>
            <table class="table table-bordered text-center" style="overflow: scroll">
                <tr>
                    <td>订单号</td>
                    <td>下单时间</td>
                    <td>订单状态</td>
                </tr>
                <?php
                $status = new Status($pdo);
                foreach ($orderInfoRows as $orderInfoRow)
                {
                    date_default_timezone_set('PRC');
                    ?>
                    <tr>
                        <td><?php echo $orderInfoRow['ordernumber'];?></td>
                        <td><?php echo $orderInfoRow['time']?></td>
                        <td>
                            <?php
                            $statusRow = $status->getStatusById($orderInfoRow['status_id']);
                            echo $statusRow['name'];
                            ?>
                        </td>
                    </tr>
                    <?
                }
                ?>
            </table>
            <?php
        }
        ?>

    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>
