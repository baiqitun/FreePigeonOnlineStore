<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 15:22
 */
require_once ("../public/common/DbConfig.php");
require_once ("../public/common/db_connect.php");
require_once ("../public/common/public_include.php");
require_once ("../public/common/classes/Touch.php");
require_once ("../public/common/classes/User.php");

session_start();
sessionIsEmptyByParamThenGotoPage('userId','index.php');
?>
<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>个人中心</title>

    <link href="../public/css/bootstrap.css" rel="stylesheet">
    <link href="css/person.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid">
    <div class="row" id="body-content">
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
            <div class="panel panel-default" id="list-panel">
                <!-- Default panel contents -->
                <div class="panel-heading text-center">账户管理</div>
                <div class="" id="account-content">
                    <div id="account-icon-body">
                        <span class="glyphicon glyphicon-user justify-content-center center-block text-center" id="account-icon-content"></span>
                    </div>
                    <div id="account-content" class="text-center">
                        <h3 class="" id="account-welcome">
                            欢迎回来:<?php
                            $user = new User($pdo);
                            $userRow = $user->getUserById($_SESSION['userId']);
                            if ($userRow != null)
                            {
                                echo "{$userRow['username']}";
                            }
                            ?>
                        </h3>
                    </div>
                </div>
                <div class="btn-group-vertical bg-default col-lg-12 col-md-12 col-sm-12 col-xs-12" role="group" aria-label="...">
                    <!-- Single button -->
                    <a class="btn btn-default btn-block" id="welcomeBtn" target="framePage" href="person/viewContactInfo.php">查看联系方式</a>
                    <a class="btn btn-default btn-block" target="framePage" href="person/addContactInfo.php">添加联系方式</a>
                    <a class="btn btn-default btn-block" target="framePage" href="person/viewOrder.php">查看订单</a>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
            <ul class="nav nav-pills navbar-right">
                <li role="presentation" class="active"><a href="index.php">首页</a></li>
                <li role="presentation"><a href="common/handle/UserLogout.php?logout=yes">注销</a></li>
                <li role="presentation"><a href="cart/index.php">查看我的购物车</a></li>
            </ul>
            <iframe src="person/viewContactInfo.php" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="framePage" name="framePage">

            </iframe>
        </div>
    </div>
</div>
<script src="../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../public/js/bootstrap.js"></script>
</body>

</html>






