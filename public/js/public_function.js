
function showPreview(source,img) {
    var file = source.files[0];
    if (window.FileReader) {
        var fr = new FileReader();
        console.log(fr);
        var portrait = document.getElementById(img);
        fr.onloadend = function (e) {
            portrait.src = e.target.result;
        };
        fr.readAsDataURL(file);
    }
}