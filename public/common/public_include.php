<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/1
 * Time: 18:37
 */

//加载所有类


//文件上传
/**
 * @param $param
 * @param $newFileName
 * @param $dir
 */
function fileUpload($param,$newFileName,$dir)
{
    $file = $_FILES[$param];
    if (is_uploaded_file($file['tmp_name']))
    {
        if (!file_exists($dir))
        {
            mkdir($dir);
        }
        $finallyFileName = $dir.'/'.$newFileName;
        if (move_uploaded_file($file['tmp_name'],$finallyFileName))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}
//2.使用MD5加密文件名并返回MD5加密后的文件名
function md5ForFileName($param)
{
    //1.获取上传文件的文件名
    $fileName = $_FILES[$param]['name'];
    //2.处理文件名称
    $fileNamePartArray = explode(".",$fileName);
    $fileExtName = array_pop($fileNamePartArray);
    $uTime = microtime(true);
    $uId = uniqid($uTime);
    $md5Uid = md5($uId);

    return $md5Uid.'.'.$fileExtName;
}
/**
 * @param $array
 */
function printArray($array)
{
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}
/**
 * @param $param
 * @return bool
 */
function noFileUpload($param)
{
    if ($_FILES[$param]['error'] == 4)
    {
        return true;
    }
    return false;
}
/**
 * @param $bool_param
 * @param $msg
 */
function checkWithMsgThenReturn($bool_param,$msg)
{
    if ($bool_param)
    {
        echo "<script>alert('{$msg}');history.go(-1);</script>";
        die();
    }
}

/**
 * @param $bool_param
 * @param $msg
 * @param $page
 */
function checkWithMsgThenGoto($bool_param,$msg,$page)
{
    if ($bool_param)
    {
        echo "<script>alert('{$msg}');location='{$page}';</script>";
        die();
    }
}

/**
 * @param $bool_param
 */
function checkTrueThenReturn($bool_param)
{
    if($bool_param)
    {
        echo "<script>history.go(-1);</script>";
        die();
    }
}

function checkFalseThenReturn($bool_param)
{
    if(!$bool_param)
    {
        echo "<script>history.go(-1);</script>";
        die();
    }
}

/**
 * @param $bool_param
 * @param $msg
 */
function checkTrueThenReturnWithMsg($bool_param,$msg)
{
    if ($bool_param)
    {
        echo "<script>alert('{$msg}');history.go(-1);</script>";
        die();
    }
}

/**
 * @param $bool_param
 * @param $msg
 */
function checkFalseThenReturnWithMsg($bool_param,$msg)
{
    if (!$bool_param)
    {
        echo "<script>alert('{$msg}');history.go(-1);</script>";
        die();
    }
}

/**
 * @param $param
 * @return bool
 */
function fileUploadHaveError($param)
{
    if ($_FILES[$param]['error'] != 0)
    {
        return true;
    }
    return false;
}
/**
 * 判断文件类型是否正确
 * @param $param
 * @param $fileTypeArray
 * @return bool
 */
function fileTypeCorrect($param,$fileTypeArray)
{
    if (in_array($_FILES[$param]['type'],$fileTypeArray))
    {
        return true;
    }
    return false;
}
/**
 * @return bool
 */
function sessionIsEmpty()
{
    if (empty($_SESSION))
    {
        return true;
    }
    return false;
}

/**
 * @param $param
 * @return bool
 */
function sessionIsEmptyByParam($param)
{
    if (empty($_SESSION[$param]))
    {
        return true;
    }
    return false;
}

function sessionIsEmptyByParamThenReturn($param)
{
    if (empty($_SESSION[$param]))
    {
        echo "<script>history.go(-1);</script>";
        die();
    }
}

function sessionIsEmptyByParamThenGotoPage($param,$page)
{
    if (empty($_SESSION[$param]))
    {
        echo "<script>location='{$page}';</script>";
        die();
    }
}

function sessionIsEmptyThenReturn()
{
    if (empty($_SESSION))
    {
        echo "<script>history.go(-1);</script>";
        die();
    }
}
/**
 * @return bool
 */
function postIsEmpty()
{
    if (empty($_POST))
    {
        return true;
    }
    return false;
}
/**
 *
 */
function postIsEmptyThenDie()
{
    if (empty($_POST))
    {
        die();
    }
}

/**
 *
 */
function postIsEmptyThenReturn()
{
    if (empty($_POST))
    {
        echo "<script>history.go(-1);</script>";
        die();
    }
}
/**
 * @param $param
 * @return bool
 */
function postIsEmptyByParam($param)
{
    if (empty($_POST[$param]))
    {
        return true;
    }
    return false;
}


/**
 * @return bool
 */
function getIsEmpty()
{
    if (empty($_GET))
    {
        return true;
    }
    return false;
}

/**
 *
 */
function getIsEmptyThenDie()
{
    if (empty($_GET))
    {
        die();
    }
}

/**
 *
 */
function getIsEmptyByParamThenReturn($param)
{
    if (empty($_GET[$param]))
    {
        echo "<script>alert({$param});history.go(-1);</script>";
        die();
    }
}
/**
 * @param $param
 * @return bool
 */
function getIsEmptyByParam($param)
{
    if (empty($_GET[$param]))
    {
        return true;
    }
    return false;
}

function getIsEmptyByParamThenGotoPage($param,$page)
{
    if (empty($_GET[$param]))
    {
        echo "<script>location='{$param}';</script>";
    }
}

/**
 * @param $msg
 */
function returnWithMsg($msg)
{
    echo "<script>alert({$msg});history.go(-1);</script>";
    die();
}

/**
 * @param $page
 */
function gotoPage($page)
{
    echo "<script>location='{$page}';</script>";
    die();
}

function gotoPageWithMsg($page,$msg)
{
    echo "<script>alert('{$msg}');location='{$page}';</script>";
    die();
}

/**
 * @param $input
 * @param $msg
 */
function textIsNullThenReturnWithMsg($text,$msg)
{
    if (trim($text) == "")
    {
        echo "<script>alert('{$msg}');history.go(-1);</script>";
        die();
    }
}

/**
 * @param $text1
 * @param $text2
 */
function textNotEqualThenReturn($text1,$text2)
{
    if (trim($text1) != trim($text2))
    {
        echo "<script>history.go(-1);</script>";
        die();
    }
}
function textEqual($text1,$text2)
{
    if (trim($text1) == trim($text2))
    {
       return true;
    }
    return false;
}
/**
 * @param $text1
 * @param $text2
 * @param $msg
 */
function textEqualThenReturnWithMsg($text1,$text2,$msg)
{
    if (trim($text1) == trim($text2))
    {
        echo "<script>alert('{$msg}');history.go(-1);</script>";
        die();
    }
}

function textNotEqualThenReturnWithMsg($text1,$text2,$msg)
{
    if (trim($text1) != trim($text2))
    {
        echo "<script>alert('{$msg}');history.go(-1);</script>";
        die();
    }
}

/**
 * @param $obj
 */
function objIsNullThenDie($obj)
{
    if($obj == null)
    {
        die();
    }
}

/**
 * @param $obj
 * @param $msg
 */
function objIsNullThenReturnWithMsg($obj,$msg)
{
    if ($obj == null)
    {
        echo "<script>alert('{$msg}');history.go(-1);</script>";
        die();
    }
}

/**
 * @param $obj1
 * @param $obj2
 * @param $msg
 */
function objNotEqualThenReturnWithMsg($obj1,$obj2,$msg)
{
    if ($obj1 != $obj2) {
        echo "<script>alert('{$msg}');history.go(-1);</script>";
        die();
    }
}

function img_create_small($big_img, $width, $height, $small_img)
{//原始大图地址，缩略图宽度，高度，缩略图地址
    $fileExt = '';
    $imgage = getimagesize($big_img); //得到原始大图片
    switch ($imgage[2]) { // 图像类型判断
        case 1:
            $im = imagecreatefromgif($big_img);
            break;
        case 2:
            $im = imagecreatefromjpeg($big_img);
            break;
        case 3:
            $im = imagecreatefrompng($big_img);
            break;
    }
    $src_W = $imgage[0]; //获取大图片宽度
    $src_H = $imgage[1]; //获取大图片高度
    $tn = imagecreatetruecolor($width, $height); //创建缩略图
    imagecopyresampled($tn, $im, 0, 0, 0, 0, $width, $height, $src_W, $src_H); //复制图像并改变大小
    imagejpeg($tn, $small_img,100); //输出图像
}
?>