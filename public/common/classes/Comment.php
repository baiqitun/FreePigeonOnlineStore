<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 17:30
 */

/**
 * Class Comment
 * Table comment
 * Note     用户评论
 */
class Comment
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param $user_id
     * @param $content
     * @param $shop_id
     * @return bool
     */
    public function addComment($user_id,$content,$shop_id)
    {
        $sql = "INSERT INTO comment(user_id,content,shop_id,time)VALUES(?,?,?,now())";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(1,$user_id);
        $stmt->bindValue(2,$content);
        $stmt->bindValue(3,$shop_id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteCommentById($id)
    {
        $sql = "DELETE FROM comment WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @param $user_id
     * @param $content
     * @param $shop_id
     * @return bool
     */
    public function modifyCommentById($id, $user_id,$content,$shop_id)
    {
        $sql = "UPDATE comment SET user_id = ?,content = ?, shop_id = ? WHERE id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$user_id);
        $stmt->bindValue(2,$$content);
        $stmt->bindValue(3,$$shop_id);
        $stmt->bindValue(4,$id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @return null|array
     */
    public function getAllComment()
    {
        $sql = "SELECT * FROM comment";
        $rowCountSql = "SELECT COUNT(*) FROM comment";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    /**
     * @param $id
     * @return null|mixed
     */
    public function getCommentById($id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM comment WHERE id = ?";
        $sql = "SELECT * FROM comment WHERE id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $row = $stmt->fetch();
                    return $row;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    public function getCommentByShopId($shop_id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM comment WHERE shop_id = ? ORDER BY time DESC";
        $sql = "SELECT * FROM comment WHERE shop_id = ? ORDER BY time DESC";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$shop_id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$shop_id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }
}