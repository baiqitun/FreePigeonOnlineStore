<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 11:04
 */

/**
 * Class Advert
 * Table advert
 * Note     广告
 */
class Advert
{
    private $pdo;

    /**
     * Advert constructor.
     * @param $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param $img
     * @param $isUsing
     * @param $shop_id
     * @param $advTitle
     * @param $advContent
     * @return bool
     */
    public function addAdvert($img,$isUsing,$shop_id,$advTitle,$advContent)
    {
        $sql = "INSERT INTO advert(img,isUsing,shop_id,advTitle,advContent)VALUES(?,?,?,?,?)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(1,$img);
        $stmt->bindValue(2,$isUsing);
        $stmt->bindValue(3,$shop_id);
        $stmt->bindValue(4,$advTitle);
        $stmt->bindValue(5,$advContent);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteAdvertById($id)
    {
        $sql = "DELETE FROM advert WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }


    /**
     * @param $id
     * @param $img
     * @param $isUsing
     * @param $shop_id
     * @param $advTitle
     * @param $advContent
     * @return bool
     */
    public function modifyAdvertById($id, $img,$isUsing,$shop_id,$advTitle,$advContent)
    {
        $sql = "UPDATE advert SET img = ?, isUsing = ?, shop_id = ? ,advTitle = ?, advContent = ? WHERE id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$img);
        $stmt->bindValue(2,$isUsing);
        $stmt->bindValue(3,$shop_id);
        $stmt->bindValue(4,$advTitle);
        $stmt->bindValue(5,$advContent);
        $stmt->bindValue(6,$id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @param $isUsing
     * @param $shop_id
     * @param $advTitle
     * @param $advContent
     * @return bool
     */
    public function modifyAdvertByIdUnlessImgField($id,$isUsing,$shop_id,$advTitle,$advContent)
    {
        $sql = "UPDATE advert SET isUsing = ?, shop_id = ?, advTitle = ?, advContent = ? WHERE id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$isUsing);
        $stmt->bindValue(2,$shop_id);
        $stmt->bindValue(3,$advTitle);
        $stmt->bindValue(4,$advContent);
        $stmt->bindValue(5,$id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @return null|array
     */
    public function getAllAdvert()
    {
        $sql = "SELECT * FROM advert";
        $rowCountSql = "SELECT COUNT(*) FROM advert";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    /**
     * @return null
     */
    public function getAllUsingAdvert()
    {
        $sql = "SELECT * FROM advert WHERE isUsing = 1";
        $rowCountSql = "SELECT COUNT(*) FROM advert WHERE isUsing = 1";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    /**
     * @param $id
     * @return null|mixed
     */
    public function getAdvertById($id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM advert WHERE id = ?";
        $sql = "SELECT * FROM advert WHERE id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $row = $stmt->fetch();
                    return $row;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    public function getAdvertImgById($id)
    {

    }
}