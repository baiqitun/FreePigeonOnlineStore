<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 7:07
 */

class OrderInfo
{
    private $pdo;

    /**
     * OrderInfo constructor.
     * @param $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param $user_id
     * @param $status_id
     * @param $touch_id
     * @return bool
     */
    public function addOrderInfo($user_id,$status_id,$touch_id,$ordernumber)
    {
        $sql = "INSERT INTO orderInfo(user_id,time,status_id,touch_id,ordernumber)VALUES(?,NOW(),?,?,?)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(1,$user_id);
        $stmt->bindValue(2,$status_id);
        $stmt->bindValue(3,$touch_id);
        $stmt->bindValue(4,$ordernumber);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteOrderInfoById($id)
    {
        $sql = "DELETE FROM orderInfo WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $status_id
     * @return bool
     */
    public function deleteOrderInfoByStatusId($status_id)
    {
        $sql = "DELETE FROM orderInfo WHERE status_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$status_id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @param $user_id
     * @param $status_id
     * @param $touch_id
     * @return bool
     */
    public function modifyOrderInfoById($id, $user_id,$status_id,$touch_id)
    {
        $sql = "UPDATE orderInfo SET user_id = ?, time = NOW(), status_id = ?, touch_id = ? WHERE id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$user_id);
        $stmt->bindValue(2,$status_id);
        $stmt->bindValue(3,$touch_id);
        $stmt->bindValue(4,$id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @return null|array
     */
    public function getAllOrderInfo()
    {
        $sql = "SELECT * FROM orderInfo";
        $rowCountSql = "SELECT COUNT(*) FROM orderInfo";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    public function getOrderInfoByUserId($user_id)
    {
        $sql = "SELECT * FROM orderInfo WHERE user_id = ? ORDER BY time DESC";
        $rowCountSql = "SELECT COUNT(*) FROM orderInfo WHERE user_id = ? ORDER BY time DESC";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$user_id);

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$user_id);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

}