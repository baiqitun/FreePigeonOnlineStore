<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/10
 * Time: 10:22
 */

/**
 * Class SoldGoods
 * Table SoldGoods
 * Noto     已出售的货物
 */
class SoldGoods
{
    private $pdo;

    /**
     * SoldGoods constructor.
     * @param $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }
    public function addSoldGoods($shop_id,$user_id)
    {
        $sql = "INSERT INTO SoldGoods(shop_id,user_id)VALUES(?,?)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(1,$shop_id);
        $stmt->bindValue(2,$user_id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    public function isFindRecordByShopIdAndUserId($shop_id,$user_id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM SoldGoods WHERE shop_id = ? AND user_id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$shop_id);
        $stmtRowCount->bindValue(2,$user_id);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }
}