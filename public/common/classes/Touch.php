<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 9:10
 */

/**
 * Class Touch
 * Table touch
 * Note     联系方式
 */
class Touch
{
    private $pdo;

    /**
     * Touch constructor.
     * @param $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }


    /**
     * @param $name
     * @param $addr
     * @param $tel
     * @param $email
     * @param $user_id
     * @return bool
     */
    public function addTouch($name,$addr,$tel,$email,$user_id)
    {
        $sql = "INSERT INTO touch(name,addr,tel,email,user_id)VALUES(?,?,?,?,?)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(1,$name);
        $stmt->bindValue(2,$addr);
        $stmt->bindValue(3,$tel);
        $stmt->bindValue(4,$email);
        $stmt->bindValue(5,$user_id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteTouchById($id)
    {
        $sql = "DELETE FROM touch WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @param $name
     * @param $addr
     * @param $tel
     * @param $email
     * @param $user_id
     * @return bool
     */
    public function modifyTouchById($id, $name,$addr,$tel,$email,$user_id)
    {
        $sql = "UPDATE touch SET name = ?,addr = ?, tel = ?,email = ?,user_id = ? WHERE id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$name);
        $stmt->bindValue(2,$addr);
        $stmt->bindValue(3,$tel);
        $stmt->bindValue(4,$email);
        $stmt->bindValue(5,$user_id);
        $stmt->bindValue(6,$id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @return null|array
     */
    public function getAllTouch()
    {
        $sql = "SELECT * FROM touch";
        $rowCountSql = "SELECT COUNT(*) FROM touch";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    /**
     * @param $id
     * @return null|mixed
     */
    public function getTouchById($id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM touch WHERE id = ?";
        $sql = "SELECT * FROM touch WHERE id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $row = $stmt->fetch();
                    return $row;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    public function getTouchByUserId($user_id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM touch WHERE user_id = ?";
        $sql = "SELECT * FROM touch WHERE user_id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$user_id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$user_id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }
}