<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 19:39
 */

/**
 * Class Brand
 * Table brand
 * Note     品牌
 */
class Brand
{
    private $pdo;

    /**
     * Brand constructor.
     * @param $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param $name
     * @param $class_id
     * @return bool
     */
    public function addBrand($name,$class_id)
    {
        $sql = "INSERT INTO brand(name,class_id)VALUES(?,?)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(1,$name);
        $stmt->bindValue(2,$class_id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteBrandById($id)
    {
        $sql = "DELETE FROM brand WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    public function deleteBrandByClassId($class_id)
    {
        $sql = "DELETE FROM brand WHERE class_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$class_id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @param $name
     * @param $class_id
     * @return bool
     */
    public function modifyBrandById($id, $name, $class_id)
    {
        $sql = "UPDATE brand SET name = ?,class_id = ? WHERE id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$name);
        $stmt->bindValue(2,$class_id);
        $stmt->bindValue(3,$id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    public function modifyAllBrandClassIdByClassId($class_id)
    {
        $sql = "UPDATE brand SET class_id = ? WHERE class_id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$class_id);
        $stmt->bindValue(2,$class_id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @return null|array
     */
    public function getAllBrand()
    {
        $sql = "SELECT * FROM brand";
        $rowCountSql = "SELECT COUNT(*) FROM brand";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    /**
     * @param $id
     * @return null|mixed
     */
    public function getBrandById($id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM brand WHERE id = ?";
        $sql = "SELECT * FROM brand WHERE id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $row = $stmt->fetch();
                    return $row;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    public function getBrandClassIdById($id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM brand WHERE id = ?";
        $sql = "SELECT class_id FROM brand WHERE id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $row = $stmt->fetch();
                    return $row;
                }
                return null;
            }
            return null;
        }
        return null;
    }


}