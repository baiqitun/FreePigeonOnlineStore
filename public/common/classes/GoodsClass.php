<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 9:48
 */

/**
 * Class GoodsClass
 * Table class
 * Note 商品分类
 */
class GoodsClass
{
    private $pdo;

    /**
     * GoodsClass constructor.
     * @param $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param $name
     * @return bool
     */
    public function addGoodsClass($name)
    {
        $sql = "INSERT INTO class(name)VALUES(?)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(1,$name);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteGoodsClassById($id)
    {
        $sql = "DELETE FROM class WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @param $name
     * @return bool
     */
    public function modifyGoodsClassById($id, $name)
    {
        $sql = "UPDATE class SET name = ? WHERE id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$name);
        $stmt->bindValue(2,$id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @return null|array
     */
    public function getAllGoodsClass()
    {
        $sql = "SELECT * FROM class";
        $rowCountSql = "SELECT COUNT(*) FROM class";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    /**
     * @param $id
     * @return null|mixed
     */
    public function getGoodsClassById($id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM class WHERE id = ?";
        $sql = "SELECT * FROM class WHERE id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $row = $stmt->fetch();
                    return $row;
                }
                return null;
            }
            return null;
        }
        return null;
    }
    public function getGoodsClassByClassId($class_id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM class WHERE class_id = ?";
        $sql = "SELECT * FROM class WHERE class_id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$class_id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$class_id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }
}