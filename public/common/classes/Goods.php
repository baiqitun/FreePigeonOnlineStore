<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/9
 * Time: 7:58
 */

/**
 * Class Shop
 * Table shop
 * Note     商品
 */
class Goods
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function addGoods($name,$img,$price,$brand_id,$class_id)
    {
        $sql = "INSERT INTO shop(name,img,price,brand_id,class_id)VALUES(?,?,?,?,?)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(1,$name);
        $stmt->bindValue(2,$img);
        $stmt->bindValue(3,$price);
        $stmt->bindValue(4,$brand_id);
        $stmt->bindValue(5,$class_id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteGoodsById($id)
    {
        $sql = "DELETE FROM shop WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $class_id
     * @return bool
     */
    public function deleteGoodsByClassId($class_id)
    {
        $sql = "DELETE FROM shop WHERE class_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$class_id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    public function deleteGoodsByBrandId($brand_id)
    {
        $sql = "DELETE FROM shop WHERE brand_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$brand_id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @param $name
     * @param $img
     * @param $price
     * @param $brand_id
     * @return bool
     */
    public function modifyGoodsById($id,$name,$img,$price,$brand_id,$class_id)
    {
        $sql = "UPDATE shop SET name = ?, img = ?, price = ? ,brand_id = ?, class_Id = ? WHERE id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$name);
        $stmt->bindValue(2,$img);
        $stmt->bindValue(3,$price);
        $stmt->bindValue(4,$brand_id);
        $stmt->bindValue(5,$class_id);
        $stmt->bindValue(6,$id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @param $name
     * @param $price
     * @param $brand_id
     * @return bool
     */
    public function modifyGoodsByIdUnlessImgField($id,$name,$price,$brand_id,$class_id)
    {
        $sql = "UPDATE shop SET name = ?, price = ?, brand_id = ?, class_id = ? WHERE id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$name);
        $stmt->bindValue(2,$price);
        $stmt->bindValue(3,$brand_id);
        $stmt->bindValue(4,$class_id);
        $stmt->bindValue(5,$id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @return null|array
     */
    public function getAllGoods()
    {
        $sql = "SELECT * FROM shop ORDER BY id DESC ";
        $rowCountSql = "SELECT COUNT(*) FROM shop ORDER BY id DESC";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    /**
     * @param $id
     * @return null|mixed
     */
    public function getGoodsById($id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM shop WHERE id = ?";
        $sql = "SELECT * FROM shop WHERE id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $row = $stmt->fetch();
                    return $row;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    public function getGoodsByLikeName($name)
    {
        $rowCountSql = "SELECT COUNT(*) FROM shop WHERE name LIKE ?";
        $sql = "SELECT * FROM shop WHERE name LIKE ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$name);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$name);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    public function getAllGoodsByClassId($class_id)
    {
        $sql = "SELECT * FROM shop WHERE class_id = ?";
        $rowCountSql = "SELECT COUNT(*) FROM shop WHERE class_id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        $stmtRowCount->bindValue(1,$class_id);
        $stmt->bindValue(1,$class_id);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }
    public function getAllGoodsByBrandId($brand_id)
    {
        $sql = "SELECT * FROM shop WHERE brand_id = ?";
        $rowCountSql = "SELECT COUNT(*) FROM shop WHERE brand_id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        $stmtRowCount->bindValue(1,$brand_id);
        $stmt->bindValue(1,$brand_id);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }
    public function getAllGoodsByBrandIdByClassId($brand_id,$class_id)
    {
        $sql = "SELECT * FROM shop WHERE brand_id = ? AND class_id = ?";
        $rowCountSql = "SELECT COUNT(*) FROM shop WHERE brand_id = ? AND class_id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        $stmtRowCount->bindValue(1,$brand_id);
        $stmtRowCount->bindValue(2,$class_id);
        $stmt->bindValue(1,$brand_id);
        $stmt->bindValue(2,$class_id);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }
}

?>