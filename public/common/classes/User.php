<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 19:36
 */

/**
 * Class User
 * Table user
 * Note     用户
 */
class User
{
    private $pdo;

    /**
     * User constructor.
     * @param $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param $username
     * @param $password
     * @return bool
     */
    public function addUser($username, $password)
    {
        $sql = "INSERT INTO user(username,password)VALUES(?,?)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindValue(1,$username);
        $stmt->bindValue(2,$password);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteUserById($id)
    {
        $sql = "DELETE FROM user WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute())
        {
            if($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $id
     * @param $password
     * @return bool
     */
    public function modifyUserById($id, $username,$password)
    {
        $sql = "UPDATE user SET username = ?, password = ? WHERE id = ?";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$username);
        $stmt->bindValue(2,$password);
        $stmt->bindValue(3,$id);

        if($stmt->execute())
        {
            if ($stmt->rowCount()>0)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @return array|null
     */
    public function getAllUser()
    {
        $sql = "SELECT * FROM user";
        $rowCountSql = "SELECT COUNT(*) FROM user";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmt = $this->pdo->prepare($sql);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $rows = $stmt->fetchAll();
                    return $rows;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    public function getUserById($id)
    {
        $rowCountSql = "SELECT COUNT(*) FROM user WHERE id = ?";
        $sql = "SELECT * FROM user WHERE id = ?";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$id);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$id);


        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {
                    $row = $stmt->fetch();
                    return $row;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    /**
     * @param $username
     * @return mixed|null
     */
    public function getUserByUsername($username)
    {
        $rowCountSql = "SELECT COUNT(*) FROM user WHERE username = ? AND isadmin = 0";
        $sql = "SELECT * FROM user WHERE username = ? AND isadmin = 0";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$username);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$username);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {

                    $row = $stmt->fetch();
                    return $row;
                }
                return null;
            }
            return null;
        }
        return null;

    }
    public function getAdminByUsername($username)
    {
        $rowCountSql = "SELECT COUNT(*) FROM user WHERE username = ? AND isadmin != 0";
        $sql = "SELECT * FROM user WHERE username = ? AND isadmin != 0";

        $stmtRowCount = $this->pdo->prepare($rowCountSql);
        $stmtRowCount->bindValue(1,$username);
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1,$username);

        if($stmtRowCount->execute())
        {
            if($stmtRowCount->fetchColumn()>0)
            {
                if($stmt->execute())
                {

                    $row = $stmt->fetch();
                    return $row;
                }
                return null;
            }
            return null;
        }
        return null;
    }
}