/*
Navicat MySQL Data Transfer

Source Server         : atguiguJDBCTest
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : free_pigeon_online_store

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-06-11 11:25:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `advert`
-- ----------------------------
DROP TABLE IF EXISTS `advert`;
CREATE TABLE `advert` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(100) NOT NULL,
  `isUsing` tinyint(4) NOT NULL,
  `shop_id` int(10) NOT NULL,
  `advTitle` varchar(100) DEFAULT NULL,
  `advContent` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of advert
-- ----------------------------
INSERT INTO `advert` VALUES ('28', '81453bfe76583fa9776318a378581b93.png', '1', '24', '', '');
INSERT INTO `advert` VALUES ('29', '5741d5e97a49671cfba6a6b8f5049f4d.png', '1', '24', '', '');
INSERT INTO `advert` VALUES ('30', '3501c9639dece96b82d24fedfaf8dec1.png', '1', '34', '小猪佩奇', '小猪佩奇身上纹，掌声送给社会人');

-- ----------------------------
-- Table structure for `brand`
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `class_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES ('5', '华为', '2');
INSERT INTO `brand` VALUES ('9', '小米', '2');
INSERT INTO `brand` VALUES ('11', '小猪佩奇', '4');
INSERT INTO `brand` VALUES ('12', '海绵宝宝', '4');
INSERT INTO `brand` VALUES ('14', '希捷', '7');
INSERT INTO `brand` VALUES ('15', '联想', '10');
INSERT INTO `brand` VALUES ('16', '花园宝宝', '4');
INSERT INTO `brand` VALUES ('17', '凹凸曼', '4');
INSERT INTO `brand` VALUES ('18', '喜羊羊', '4');
INSERT INTO `brand` VALUES ('20', '西部数据', '7');
INSERT INTO `brand` VALUES ('21', '高通', '8');
INSERT INTO `brand` VALUES ('22', '英伟达', '8');
INSERT INTO `brand` VALUES ('23', '龙芯', '7');
INSERT INTO `brand` VALUES ('27', 'Linux', '8');
INSERT INTO `brand` VALUES ('29', '联想', '10');
INSERT INTO `brand` VALUES ('30', '红富士', '11');

-- ----------------------------
-- Table structure for `class`
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class
-- ----------------------------
INSERT INTO `class` VALUES ('2', '手机');
INSERT INTO `class` VALUES ('4', '吉祥物');
INSERT INTO `class` VALUES ('7', '硬盘');
INSERT INTO `class` VALUES ('8', '芯片');
INSERT INTO `class` VALUES ('10', '电脑');
INSERT INTO `class` VALUES ('11', '苹果');

-- ----------------------------
-- Table structure for `comment`
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` text,
  `shop_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('4', '13', '               水电费', '24', '2018-06-10 18:24:50');
INSERT INTO `comment` VALUES ('5', '11', '这是user的评论  ', '25', '2018-06-10 18:28:04');
INSERT INTO `comment` VALUES ('6', '11', '           很可爱的公仔！     ', '32', '2018-06-10 18:30:30');
INSERT INTO `comment` VALUES ('7', '11', '                可以', '32', '2018-06-10 18:31:23');
INSERT INTO `comment` VALUES ('8', '11', '                可以', '32', '2018-06-10 18:31:39');
INSERT INTO `comment` VALUES ('9', '11', '                是大法官', '32', '2018-06-10 18:32:32');
INSERT INTO `comment` VALUES ('10', '11', '                ', '32', '2018-06-10 18:34:04');
INSERT INTO `comment` VALUES ('11', '11', '                很不错', '33', '2018-06-10 18:43:15');

-- ----------------------------
-- Table structure for `orderinfo`
-- ----------------------------
DROP TABLE IF EXISTS `orderinfo`;
CREATE TABLE `orderinfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL,
  `touch_id` int(11) NOT NULL,
  `ordernumber` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orderinfo
-- ----------------------------
INSERT INTO `orderinfo` VALUES ('23', '13', '2018-06-10 18:16:33', '1', '6', '948f405a86b438c7d72850ba9b2f2b7f');
INSERT INTO `orderinfo` VALUES ('24', '11', '2018-06-10 18:27:36', '1', '5', '295b0a26f126f1ccf08f4ea50af84b4e');
INSERT INTO `orderinfo` VALUES ('25', '11', '2018-06-10 18:29:49', '1', '4', '3e7313ae8a0fcfa8bfc3f4767be709ca');
INSERT INTO `orderinfo` VALUES ('26', '11', '2018-06-10 18:42:25', '1', '3', '4208a5ec9b1c1a7f5fc61435ecfafde2');
INSERT INTO `orderinfo` VALUES ('27', '11', '2018-06-10 18:59:47', '1', '7', 'c85e589fa2d5456aaced9c4493afddb6');

-- ----------------------------
-- Table structure for `shop`
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `img` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `brand_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop
-- ----------------------------
INSERT INTO `shop` VALUES ('24', '荣耀 P1', '6f098f9841f44d71a552cc33a1c22311.jpg', '20000', '5', '2');
INSERT INTO `shop` VALUES ('25', '荣耀 P2', '2a96c051a10d5396960e145b3b53365e.JPG', '2002', '5', '2');
INSERT INTO `shop` VALUES ('26', 'TP 1X', '9404829acd8ead7fb25a24dbda99669f.jpg', '4000', '29', '10');
INSERT INTO `shop` VALUES ('27', 'TP 1X', 'f24519c967241979c7d59fb3a4081a3d.jpg', '4000', '29', '10');
INSERT INTO `shop` VALUES ('30', '晓龙 820', '4882aa9460aaf7ba5f51acb6f375dd95.jpg', '1500', '21', '8');
INSERT INTO `shop` VALUES ('31', '晓龙 825', '61e1fce1583f3876fcc44bb61cf16d29.jpg', '2000', '21', '8');
INSERT INTO `shop` VALUES ('32', '喜羊羊', '7f7dcd34006a366f55a10976fb28a110.jpg', '200', '18', '4');
INSERT INTO `shop` VALUES ('33', '灰太狼', '94657d92ed6307eb02efb2e56d3c21fa.jpg', '120', '18', '4');
INSERT INTO `shop` VALUES ('34', '小猪佩奇', '6681753b3d94bc5e9960b67ae42d94bc.jpg', '400', '11', '4');
INSERT INTO `shop` VALUES ('35', 'MIX ', 'b5b4b694cf12db6c2e58edbcf8938e70.jpg', '2755', '9', '2');
INSERT INTO `shop` VALUES ('36', 'MI5 Plus', '11c652f6a7a0329402fc52b21207e22c.jpg', '1755', '9', '2');
INSERT INTO `shop` VALUES ('37', 'CoManger-1x', 'f80cf85bf4585af279d21b457a957837.jpg', '4000', '20', '7');
INSERT INTO `shop` VALUES ('38', '红富士 11x', 'e47775ed299601dd08c792a3140013ba.jpg', '12', '30', '11');

-- ----------------------------
-- Table structure for `soldgoods`
-- ----------------------------
DROP TABLE IF EXISTS `soldgoods`;
CREATE TABLE `soldgoods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `orderinfo_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soldgoods
-- ----------------------------
INSERT INTO `soldgoods` VALUES ('1', '24', '13', null);
INSERT INTO `soldgoods` VALUES ('2', '25', '11', null);
INSERT INTO `soldgoods` VALUES ('3', '34', '11', null);
INSERT INTO `soldgoods` VALUES ('4', '32', '11', null);
INSERT INTO `soldgoods` VALUES ('5', '35', '11', null);
INSERT INTO `soldgoods` VALUES ('6', '33', '11', null);
INSERT INTO `soldgoods` VALUES ('7', '26', '11', null);
INSERT INTO `soldgoods` VALUES ('8', '31', '11', null);

-- ----------------------------
-- Table structure for `status`
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of status
-- ----------------------------
INSERT INTO `status` VALUES ('1', '进行中');
INSERT INTO `status` VALUES ('2', '已成功');

-- ----------------------------
-- Table structure for `touch`
-- ----------------------------
DROP TABLE IF EXISTS `touch`;
CREATE TABLE `touch` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `addr` varchar(100) NOT NULL,
  `tel` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of touch
-- ----------------------------
INSERT INTO `touch` VALUES ('1', '泰罗奥特曼', 'M78星云', 'm78-65536', 'tailuo@m78.com', '9');
INSERT INTO `touch` VALUES ('2', '泰罗奥特曼', 'M78星云', '111', '34@sd', '9');
INSERT INTO `touch` VALUES ('3', '1', '1', '1', '1', '11');
INSERT INTO `touch` VALUES ('4', '2', '2', '2', '333', '11');
INSERT INTO `touch` VALUES ('6', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `isadmin` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '1', '1');
INSERT INTO `user` VALUES ('9', 'tailuo', '2', '0');
INSERT INTO `user` VALUES ('10', 'user1', 'user', '0');
INSERT INTO `user` VALUES ('11', 'user', 'user', '0');
INSERT INTO `user` VALUES ('13', '12', '1', '0');
INSERT INTO `user` VALUES ('14', '1111', '1111', '0');
INSERT INTO `user` VALUES ('15', 'user@user.com', '111', '0');
INSERT INTO `user` VALUES ('16', 'user1@user.com', '111', '0');
INSERT INTO `user` VALUES ('17', 'user11@user.com', '1', '0');
INSERT INTO `user` VALUES ('18', '11@ASDF', '11', '0');
INSERT INTO `user` VALUES ('19', '1@1.com', '1', '0');
