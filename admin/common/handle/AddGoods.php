<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/9
 * Time: 8:38
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Goods.php");
require_once ("../../../public/common/classes/Brand.php");
require_once ("../../../public/common/classes/GoodsClass.php");

session_start();

postIsEmptyThenReturn();
sessionIsEmptyByParamThenReturn('adminId');
checkTrueThenReturnWithMsg(noFileUpload('img'),'请选择一张图片！');
checkFalseThenReturnWithMsg(
    fileTypeCorrect('img', array('image/jpeg','image/png','image/gif')),
    '文件格式不正确:请上传jpg/png/gif格式的图片');
checkWithMsgThenReturn(fileUploadHaveError('img'),'文件上传失败！');

printArray($_FILES);
printArray($_POST);

$md5FileName = md5ForFileName('img');
checkFalseThenReturnWithMsg(fileUpload('img',$md5FileName,
    '../../../public/img/GoodsImg/real'),'文件上传失败！');
$goods = new Goods($pdo);
$brand = new Brand($pdo);

$goodsName = $_POST['goodsName'];
$goodsBrandId = $_POST['goodsBrand'];
$goodsPrice = $_POST['goodsPrice'];

$brandRow = $brand->getBrandById($goodsBrandId);
$brandClassId = $brandRow['class_id'];

textIsNullThenReturnWithMsg($goodsName,'请输入商品名称！');
textIsNullThenReturnWithMsg($goodsPrice,'请输入商品价格！');

img_create_small("../../../public/img/GoodsImg/real/{$md5FileName}",
    100,100,
    "../../../public/img/GoodsImg/small/{$md5FileName}");
if (!$goods->addGoods($goodsName,$md5FileName,floatval($goodsPrice),intval($goodsBrandId),intval($brandClassId)))
{
    returnWithMsg('添加失败');
}
gotoPageWithMsg('../../admin/viewGoods.php','添加成功！');
?>