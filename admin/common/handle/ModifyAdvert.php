<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/8
 * Time: 20:54
 */
require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Advert.php");

session_start();

postIsEmptyThenReturn();
sessionIsEmptyByParamThenReturn('adminId');

$advertId = $_POST['id'];
$shopId = $_POST['shopId'];
$advTitle = $_POST['advTitle'];
$advContent = $_POST['advContent'];
$isUsing = $_POST['isUsing'];
$advert = new Advert($pdo);

$isUsingFlag = 0;
if ($isUsing == 'yes')
{
    $isUsingFlag = 1;
}


if (noFileUpload('img'))
{
    if (!$advert->modifyAdvertByIdUnlessImgField($advertId,$isUsingFlag,intval($shopId),$advTitle,$advContent))
    {
        returnWithMsg('修改失败！');
       echo "修改失败！";
    }
    gotoPageWithMsg('../../admin/viewAdvert.php','修改成功！');
}
else
{
    if (!fileTypeCorrect('img',array('image/jpeg','image/png','image/gif')))
    {
        returnWithMsg('图片格式不正确！');
    }
    $md5FileName = md5ForFileName('img');
    fileUpload('img',$md5FileName,'../../../public/img/AdvertImg');
    if (!$advert->modifyAdvertById($advertId,$md5FileName,$isUsingFlag,intval($shopId),$advTitle,$advContent))
    {
        returnWithMsg('修改失败！');
    }
    gotoPageWithMsg('../../admin/viewAdvert.php','修改成功！');
}