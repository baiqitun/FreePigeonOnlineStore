<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 21:52
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/User.php");

session_start();

postIsEmptyThenReturn();
sessionIsEmptyByParamThenReturn('adminId');

$userId = $_POST['id'];
$username = $_POST['username'];
$password = $_POST['password'];
$confirmPassword = $_POST['confirmPassword'];

textIsNullThenReturnWithMsg($username,'用户名不能为空！');
textIsNullThenReturnWithMsg($password,'密码不能为空！');
textIsNullThenReturnWithMsg($confirmPassword,'确认密码不能为空！');
textNotEqualThenReturnWithMsg($password,$confirmPassword,'两次密码输入不一致！');

$user = new User($pdo);
$userRow = $user->getUserByUsername($username);
if ($userRow != null)
{
    returnWithMsg('用户名已存在！');
}
if (!$user->modifyUserById($userId,$username,$password))
{
    returnWithMsg('您没有修改任何数据！');
}
gotoPageWithMsg('../../admin/viewUser.php','修改成功！');