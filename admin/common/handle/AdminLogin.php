<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 19:41
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/User.php");

session_start();

$username = $_POST['username'];
$password = $_POST['password'];

textIsNullThenReturnWithMsg($username,'用户名不能为空！');
textIsNullThenReturnWithMsg($password,'密码不能为空！');

$admin = new User($pdo);
$adminRow = $admin->getAdminByUsername($username);

objNotEqualThenReturnWithMsg($password,$adminRow['password'],'用户名或密码错误！');

$_SESSION['adminUsername'] = $adminRow['username'];
$_SESSION['adminId']  = $adminRow['id'];

gotoPageWithMsg("../../index.php",'登录成功！')
?>