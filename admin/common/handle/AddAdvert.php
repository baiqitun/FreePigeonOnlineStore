<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/6
 * Time: 7:26
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Advert.php");

session_start();
sessionIsEmptyByParamThenReturn('adminId');
checkTrueThenReturnWithMsg(noFileUpload('img'),'请选择一张图片！');
checkFalseThenReturnWithMsg(fileTypeCorrect('img',array('image/jpeg','image/png','image/gif')),'文件格式不正确:请上传jpg/png/gif格式的图片');
checkWithMsgThenReturn(fileUploadHaveError('img'),'文件上传失败！');

printArray($_FILES);
printArray($_POST);


$md5FileName = md5ForFileName('img');
checkFalseThenReturnWithMsg(fileUpload('img',$md5FileName,'../../../public/img/AdvertImg'),'文件上传失败！');
$advert = new Advert($pdo);


$advTitle = $_POST['advTitle'];
$advContent = $_POST['advContent'];
$isUsing = $_POST['isUsing'];
$shopId = $_POST['shopId'];

$isUsingFlag = 0;
if ($isUsing == 'yes')
{
    $isUsingFlag = 1;
}
if(!$advert->addAdvert($md5FileName,$isUsingFlag,intval($shopId),$advTitle,$advContent))
{
    returnWithMsg('添加失败！');
}
gotoPageWithMsg('../../admin/viewAdvert.php','添加成功！');

