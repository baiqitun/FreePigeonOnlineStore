<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 12:20
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Advert.php");

session_start();
getIsEmptyByParamThenReturn('id');
sessionIsEmptyByParamThenReturn('adminId');

$advertId = $_GET['id'];
$advert = new Advert($pdo);
$advertRow = $advert->getAdvertById(intval($advertId));
if (!$advert->deleteAdvertById(intval($advertId)))
{
    returnWithMsg('删除失败！');
}

@unlink("../../../public/img/AdvertImg/{$advertRow['img']}");

gotoPageWithMsg('../../admin/viewAdvert.php','删除成功！');