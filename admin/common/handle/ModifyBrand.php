<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 8:04
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Brand.php");

session_start();

postIsEmptyThenReturn();
sessionIsEmptyByParamThenReturn('adminId');

$brandId = $_POST['id'];
$brandName = $_POST['brandName'];
$brandClassId = $_POST['class'];
$brand = new Brand($pdo);

textIsNullThenReturnWithMsg($brandName,'品牌名称不能为空！');

if (!$brand->modifyBrandById(intval($brandId),$brandName,$brandClassId))
{
    returnWithMsg('修改失败！');
}
gotoPageWithMsg('../../admin/viewBrand.php','修改成功！');