<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 21:52
 */
require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/User.php");

session_start();
getIsEmptyByParamThenReturn('id');
sessionIsEmptyByParamThenReturn('adminId');

$userId = $_GET['id'];
$user = new User($pdo);

if(!$user->deleteUserById(intval($userId)))
{
    gotoPageWithMsg('../../admin/viewUser.php','删除失败！');
}
gotoPageWithMsg('../../admin/viewUser.php','删除成功！');