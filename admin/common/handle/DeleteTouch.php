<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 13:29
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Touch.php");


session_start();
getIsEmptyByParamThenReturn('id');
sessionIsEmptyByParamThenReturn('adminId');

$touchId = $_GET['id'];
$touch = new Touch($pdo);
if (!$touch->deleteTouchById(intval($touchId)))
{
    returnWithMsg('删除失败！');
}
gotoPageWithMsg('../../admin/viewTouch.php','删除成功！');
?>

