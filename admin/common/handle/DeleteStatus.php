<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 7:36
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Status.php");
require_once ("../../../public/common/classes/OrderInfo.php");

session_start();
getIsEmptyByParamThenReturn('id');
sessionIsEmptyByParamThenReturn('adminId');

$statusId = $_GET['id'];

$status = new Status($pdo);
$deleteStatus = $status->deleteStatusById(intval($statusId));
//$orderInfo = new OrderInfo($pdo);
//$deleteOrderInfo = $orderInfo->deleteOrderInfoByStatusId(intval($statusId));
//&& $deleteOrderInfo
if (!($deleteStatus ))
{
    returnWithMsg('删除失败！');
}
gotoPageWithMsg('../../admin/viewStatus.php','删除成功！');