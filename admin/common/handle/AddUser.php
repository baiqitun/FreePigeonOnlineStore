<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 8:40
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/User.php");

session_start();

postIsEmptyThenReturn();
sessionIsEmptyByParamThenReturn('adminId');

$username = $_POST['username'];
$password = $_POST['password'];
$confirmPassword = $_POST['confirmPassword'];

textIsNullThenReturnWithMsg($username,'用户名不能为空！');
textIsNullThenReturnWithMsg($password,'密码不能为空！');
textIsNullThenReturnWithMsg($confirmPassword,'确认密码不能为空！');
textNotEqualThenReturnWithMsg($password,$confirmPassword,'两次密码输入不一致！');

$user = new User($pdo);
$userRow = $user->getUserByUsername("$username");
if ($userRow != null)
{
    echo "<script>alert('用户名已存在！');history.go(-1);</script>";
    die();
}

if(!$user->addUser($username,$password))
{
    returnWithMsg('添加失败！');

}
gotoPageWithMsg('../../admin/viewUser.php','添加成功!');
