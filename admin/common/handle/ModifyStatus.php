<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 8:13
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Status.php");

session_start();

postIsEmptyThenReturn();
sessionIsEmptyByParamThenReturn('adminId');

$statusId = $_POST['id'];
$statusName = $_POST['name'];
$status = new Status($pdo);

textIsNullThenReturnWithMsg($statusName,'订单状态不能为空！');

if (!$status->modifyStatusById($statusId,$statusName))
{
    returnWithMsg('修改失败！');
}
gotoPageWithMsg('../../admin/viewStatus.php','修改成功！');
?>