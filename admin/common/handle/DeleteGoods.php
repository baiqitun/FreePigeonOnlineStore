<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/9
 * Time: 9:18
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Goods.php");;

session_start();
getIsEmptyByParamThenReturn('id');
sessionIsEmptyByParamThenReturn('adminId');

$goodsId=$_GET['id'];
$good = new Goods($pdo);

if (!$good->deleteGoods(intval($goodsId)))
{
    returnWithMsg('删除失败！');
}
gotoPageWithMsg('../../admin/viewGoods.php','删除成功！');
?>