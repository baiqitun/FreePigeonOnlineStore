<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 10:17
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/GoodsClass.php");

session_start();

postIsEmptyThenReturn();
sessionIsEmptyByParamThenReturn('adminId');

$goodsClassName = $_POST['goodsClassName'];

textIsNullThenReturnWithMsg($goodsClassName,'分类名称不能为空！');

$goodsClass = new GoodsClass($pdo);
if (!$goodsClass->addGoodsClass($goodsClassName))
{
    returnWithMsg('添加失败！');
}
gotoPageWithMsg('../../admin/viewClass.php','添加成功！');
