<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 10:07
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/GoodsClass.php");
require_once ("../../../public/common/classes/Brand.php");

session_start();
getIsEmptyByParamThenReturn('id');
sessionIsEmptyByParamThenReturn('adminId');

$goodsClassId = $_GET['id'];
$goodsClass = new GoodsClass($pdo);
$brand = new Brand($pdo);
$goods = new Goods($pdo);
$brandClassId = $goodsClassId;

//级联删除
$deleteClass = $goodsClass->deleteGoodsClassById(intval($goodsClassId));
$deleteBrand = $brand->deleteBrandByClassId($brandClassId);
$deleteGoods = $goods->deleteGoodsByClassId($brandClassId);

if ($deleteClass && $deleteBrand && $deleteGoods)
{
    returnWithMsg('删除失败！');
}
gotoPageWithMsg('../../admin/viewClass.php','删除成功！');