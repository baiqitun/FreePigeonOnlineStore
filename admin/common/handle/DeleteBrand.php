<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 21:02
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Brand.php");

session_start();
getIsEmptyByParamThenReturn('id');
sessionIsEmptyByParamThenReturn('adminId');

$brandId = $_GET['id'];
$brand = new Brand($pdo);
$goods = new Goods($pdo);

$deleteBrand = $goods->deleteGoodsByBrandId($brandId);
$deleteGoods = $brand->deleteBrandById(intval($brandId));

if (!($deleteBrand && $deleteGoods))
{
    returnWithMsg('删除失败！');
}
gotoPageWithMsg('../../admin/viewBrand.php','删除成功！');
?>