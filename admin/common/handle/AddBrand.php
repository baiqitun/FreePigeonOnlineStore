<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 20:20
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/GoodsClass.php");
require_once ("../../../public/common/classes/Brand.php");

session_start();

postIsEmptyThenReturn();
sessionIsEmptyByParamThenReturn('adminId');

$name = $_POST['name'];
$classId = $_POST['class'];
textIsNullThenReturnWithMsg($name,'品牌名称不能为空！');
textIsNullThenReturnWithMsg($classId,'所属类型不能为空！');

$brand = new Brand($pdo);
if(!$brand->addBrand($name,intval($classId)))
{
    returnWithMsg('添加失败！');
}
gotoPageWithMsg('../../admin/viewBrand.php','添加成功！');
?>