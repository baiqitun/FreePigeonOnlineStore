<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 20:53
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");

session_start();

textNotEqualThenReturn($_GET['logout'],'yes');
sessionIsEmptyByParamThenReturn('adminId');

$_SESSION = array();
setcookie('PHPSESSID','',time()-1,'/');

gotoPageWithMsg('../../login.php','注销成功！');
?>