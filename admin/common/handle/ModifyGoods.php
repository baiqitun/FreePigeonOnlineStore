<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/9
 * Time: 11:10
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Goods.php");
require_once ("../../../public/common/classes/Brand.php");

session_start();

postIsEmptyThenReturn();
sessionIsEmptyByParamThenReturn('adminId');

$goodsId = $_POST['id'];
$goodsName = $_POST['goodsName'];
$goodsPrice = $_POST['goodsPrice'];
$goodsBrandId = $_POST['goodsBrand'];

$goods = new Goods($pdo);

textIsNullThenReturnWithMsg($goodsName,'请输入商品名称！');
textIsNullThenReturnWithMsg($goodsPrice,'请输入商品价格！');

printArray($_POST);
printArray($_FILES);

$brand = new Brand($pdo);
$brandRow = $brand->getBrandById($goodsBrandId);
$brandClassId = $brandRow['class_id'];
echo "$brandClassId";
if (noFileUpload('img'))
{
    if (!$goods->modifyGoodsByIdUnlessImgField($goodsId,$goodsName,$goodsPrice,$goodsBrandId,$brandClassId))
    {
        echo "<script>alert('修改失败！');history.go(-1);</script>";
        die();
    }
    gotoPageWithMsg('../../admin/viewGoods.php','修改成功！');
}
else
{
    if (!fileTypeCorrect('img',array('image/jpeg','image/png','image/gif')))
    {
        returnWithMsg('图片格式不正确！');
    }
    $md5FileName = md5ForFileName('img');
    if (fileUpload('img',$md5FileName,'../../../public/img/GoodsImg/real'))
    {
        printArray($_FILES);
        img_create_small("../../../public/img/GoodsImg/real/{$md5FileName}",
            100,100,
            "../../../public/img/GoodsImg/small/{$md5FileName}");
        if (!$goods->modifyGoodsById($goodsId,$goodsName,$md5FileName,$goodsPrice,$goodsBrandId,intval($brandClassId)))
        {
            returnWithMsg('修改失败！');
        }
        gotoPageWithMsg('../../admin/viewGoods.php','修改成功！');
    }
}
?>