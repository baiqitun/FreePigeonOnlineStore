<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 10:37
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/GoodsClass.php");

session_start();

postIsEmptyThenReturn();
sessionIsEmptyByParamThenReturn('adminId');

$goodsClassId = $_POST['id'];
$goodsClassName = $_POST['goodsClassName'];

textIsNullThenReturnWithMsg($goodsClassName,'分类名称不能为空！');
$goodsClass = new GoodsClass($pdo);

if (!$goodsClass->modifyGoodsClassById($goodsClassId,$goodsClassName))
{
    returnWithMsg('修改失败！');
}
gotoPageWithMsg('../../admin/viewClass.php','修改成功！');
?>