
<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/10
 * Time: 19:21
 */

require_once ("../../../public/common/DbConfig.php");
require_once ("../../../public/common/db_connect.php");
require_once ("../../../public/common/public_include.php");
require_once ("../../../public/common/classes/Brand.php");
require_once ("../../../public/common/classes/Comment.php");

session_start();
getIsEmptyByParamThenReturn('id');
sessionIsEmptyByParamThenReturn('adminId');

$commentId = $_GET['id'];
$comment = new Comment($pdo);
if (!$comment->deleteCommentById($commentId))
{
    returnWithMsg('删除失败！');
}
gotoPageWithMsg('../../admin/viewComment.php','删除成功！');
?>