<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/9
 * Time: 8:11
 */

require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Brand.php");
require_once ("../../public/common/classes/Goods.php");

session_start();
sessionIsEmptyByParamThenGotoPage('adminId','../login.php');
?>

<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>添加商品</title>

    <link href="../../public/css/bootstrap.css" rel="stylesheet">
    <link href="../css/View-addUser.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid" id="main-body">
    <div class="row text-center" id="main-body">
        <div class="panel panel-default col-md-3 col-lg-3 " id="add-user-panel">
            <!-- Default panel contents -->
            <div class="panel-heading">添加商品</div>
            <div class="panel-body">
                <form class="form-horizontal" action="../common/handle/AddGoods.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-sm-12" >
                            <input type="text" class="form-control" id="input-goodsName" placeholder="商品名称" name="goodsName">
                        </div>
                    </div>
                    <div class="form-group">
                        <img src="../../public/img/default-user.png" class="col-sm-8 col-lg-offset-2" id="form-goodsImg">
                        <div class="col-sm-12">
                            <input type="file" class="form-control" id="input-file" placeholder="图片" name="img" onchange="showPreview(this,'form-goodsImg')">
                        </div>
                    </div>
                    <img src="" alt="">
                    <div class="form-group">
                        <div class="col-sm-12" >
                            <input type="text" class="form-control" id="input-goodsPrice" placeholder="商品价格" name="goodsPrice">
                        </div>
                    </div>
                    <div class="form-control">
                        <?php
                        $brand = new Brand($pdo);
                        $brandBows = $brand->getAllBrand();
                        ?>
                        <div class="col-sm-12">
                            <label for="input-goodsBrand">所属品牌</label>
                            <select id="input-goodsBrand" name="goodsBrand">
                                <?php
                                if ($brandBows != null)
                                {
                                    foreach ($brandBows as $row)
                                    {
                                        ?>
                                        <option value="<?php echo $row['id']?>"><?php echo $row['name']?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class=" col-lg-12  col-md-12">
                            <button type="submit" class="btn btn-default">确认添加</button>
                            <a class="btn btn-default" href="viewAdvert.php">取消操作</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
<script src="../../public/js/public_function.js"></script>
</body>
