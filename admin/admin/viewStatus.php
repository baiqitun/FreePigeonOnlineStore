<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 7:30
 */

require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Status.php");

session_start();
sessionIsEmptyByParamThenGotoPage('adminId','../login.php');

?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>查看订单状态</title>
    <link href="../../public/css/bootstrap.css" rel="stylesheet">
</head>

<body style="margin-top: 10px;">
<div class="container-fluid" id="main-body">
    <div class="row" id="main-body">
        <table class="table table-bordered text-center" style="overflow: scroll">
            <tr>
                <td>编号</td>
                <td>订单状态名称</td>
                <td>操作</td>
            </tr>
            <?php
            $status = new Status($pdo);
            $statusRows = $status->getAllStatus();
            if ($statusRows != null)
            {
                foreach ($statusRows as $row)
                {
                    ?>
                    <tr>
                        <td><?php echo "{$row['id']}";?></td>
                        <td><?php echo "{$row['name']}";?></td>
                        <td>
                            <a class="pull-left" href="modifyStatus.php?id=<?php echo $row['id'];?>">修改</a>
                            <a class="pull-right" href="../common/handle/DeleteStatus.php?id=<?php echo $row['id'];?>">删除</a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>
