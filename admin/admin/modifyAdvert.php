<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 11:57
 */
require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Advert.php");
require_once ("../../public/common/classes/Goods.php");

session_start();
sessionIsEmptyByParamThenGotoPage('adminId','../login.php');
getIsEmptyByParamThenReturn('id');

$advertId = $_GET['id'];
$advert = new Advert($pdo);
$advertRow = $advert->getAdvertById(intval($advertId));
?>

<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>修改广告</title>

    <link href="../../public/css/bootstrap.css" rel="stylesheet">
    <link href="../css/View-addUser.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid" id="main-body">
    <div class="row text-center" id="main-body">
        <div class="panel panel-default col-md-3 col-lg-3 " id="add-user-panel">
            <!-- Default panel contents -->
            <div class="panel-heading">修改广告</div>
            <div class="panel-body">
                <form class="form-horizontal" action="../common/handle/ModifyAdvert.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $advertRow['id']?>" name="id">
                    <div class="form-group">
                        <img src="../../public/img/AdvertImg/<?php echo "{$advertRow['img']}";?>" class="col-sm-8 col-lg-offset-2" id="form-bannerImg">
                        <div class="col-sm-12">
                            <input type="file" class="form-control" id="input-file" placeholder="图片" name="img" onchange="showPreview(this,'form-bannerImg')">
                        </div>
                    </div>
                    <img src="" alt="">
                    <div class="form-group">
                        <div class="col-sm-12" >
                            <input type="text" class="form-control" id="input-advTitle" placeholder="广告标题" name="advTitle"
                            value="<?php echo "{$advertRow['advTitle']}";?>"
                            >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12" >
                            <textarea class="form-control" id="input-affirm-password" placeholder="广告内容" name="advContent"><?php
                                echo "{$advertRow['advContent']}"; ?>
                            </textarea>
                        </div>
                    </div>
                    <label for="isUsing">是否将此广告设为轮播</label>
                    <div class="form-control">
                        <div class="col-sm-12">
                            <select id="isUsing" name="isUsing">
                                <option value="yes"<?php
                                if ($advertRow['isUsing'] == 1)
                                {
                                    echo "selected";
                                }
                                ?>>是</option>
                                <option value="no"<?php
                                if ($advertRow['isUsing'] == 0)
                                {
                                    echo "selected";
                                }
                                ?>>否</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-control">
                        <div class="col-sm-12">

                            <?php
                            $goods = new Goods($pdo);
                            $goodsRows = $goods->getAllGoods();
                            ?>
                            <label for="input-shop">宣传商品</label>
                            <select id="input-shop" name="shopId">
                                <?php
                                if ($goodsRows != null)
                                {
                                    echo $advertId;
                                    foreach ($goodsRows as $goodsRow)
                                    {
                                        ?>
                                        <option value="<?php echo $goodsRow['id'];?>" <?php if ($goodsRow['id'] == $advertRow['shop_id']){echo "selected";}?> ><?php echo $goodsRow['name']?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class=" col-lg-12  col-md-12">
                            <button type="submit" class="btn btn-default">确认添加</button>
                            <a class="btn btn-default" href="viewAdvert.php">取消操作</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
<script src="../../public/js/public_function.js"></script>
</body>
