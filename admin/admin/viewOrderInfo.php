<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 8:56
 */

require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Status.php");
require_once ("../../public/common/classes/OrderInfo.php");

session_start();
sessionIsEmptyByParamThenGotoPage('adminId','../login.php');
?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>查看订单状态</title>
    <link href="../../public/css/bootstrap.css" rel="stylesheet">
</head>

<body style="margin-top: 10px;">
<div class="container-fluid" id="main-body">
    <div class="row" id="main-body">
        <table class="table table-bordered text-center" style="overflow: scroll">
            <tr>
                <td>编号</td>
                <td>订单号</td>
                <td>发起时间</td>
                <td>订单状态</td>
                <td>用户</td>

            </tr>
            <?php
            //多表联合查询
            $sql = "SELECT orderinfo.id as orderId,orderinfo.ordernumber as orderNum,orderinfo.time as orderTime,`status`.`name` as statusName,`user`.username as username FROM orderinfo,`status`,`user` WHERE orderinfo.status_id = `status`.id AND orderinfo.user_id = `user`.id ORDER BY time DESC";
            $rowCountSql = "SELECT COUNT(*) FROM orderinfo,`status`,`user` WHERE orderinfo.status_id = `status`.id AND orderinfo.user_id = `user`.id ORDER BY time DESC";

            $stmtRowCount = $pdo->prepare($rowCountSql);
            $stmt = $pdo->prepare($sql);

            $orderInfoWithStatusWithTouchRows = null;
            if($stmtRowCount->execute())
            {
                if($stmtRowCount->fetchColumn()>0)
                {
                    if($stmt->execute())
                    {
                        $orderInfoWithStatusWithTouchRows = $stmt->fetchAll();
                    }
                }
            }

            if ($orderInfoWithStatusWithTouchRows != null)
            {
                foreach ($orderInfoWithStatusWithTouchRows as $row)
                {
                    ?>
                    <tr>
                        <td><?php echo "{$row['orderId']}"?></td>
                        <td><?php echo "{$row['orderNum']}"?></td>
                        <td><?php echo "{$row['orderTime']}"?></td>
                        <td><?php echo "{$row['statusName']}"?></td>
                        <td><?php echo "{$row['username']}"?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>
