<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/4
 * Time: 9:21
 */

require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Touch.php");
session_start();
sessionIsEmptyByParamThenGotoPage('adminId','../login.php');
?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>查看品牌</title>
    <link href="../../public/css/bootstrap.css" rel="stylesheet">
</head>

<body style="margin-top: 10px;">
<div class="container-fluid" id="main-body">
    <div class="row" id="main-body">
        <table class="table table-bordered text-center" style="overflow: scroll">
            <tr>
                <td>编号</td>
                <td>姓名</td>
                <td>地址</td>
                <td>电话号码</td>
                <td>电子邮箱</td>
                <td>用户名</td>
                <td>操作</td>
            </tr>
            <?php
            //多表联合查询
            $sql = "SELECT touch.id,touch.name,addr,tel,email,`user`.username FROM touch,`user` WHERE touch.user_id = `user`.id";
            $rowCountSql = "SELECT COUNT(*) FROM touch,`user` WHERE touch.user_id = `user`.id";

            $stmtRowCount = $pdo->prepare($rowCountSql);
            $stmt = $pdo->prepare($sql);

            $touchAndUserRows = null;
            if($stmtRowCount->execute())
            {
                if($stmtRowCount->fetchColumn()>0)
                {
                    if($stmt->execute())
                    {
                        $touchAndUserRows = $stmt->fetchAll();
                    }
                }
            }
            if ($touchAndUserRows != null)
            {
                foreach ($touchAndUserRows as $row)
                {
                    ?>
                    <tr>
                        <td><?php echo "{$row['id']}";?></td>
                        <td><?php echo "{$row['name']}";?></td>
                        <td><?php echo "{$row['addr']}"?></td>
                        <td><?php echo "{$row['tel']}"?></td>
                        <td><?php echo "{$row['email']}"?></td>
                        <td><?php echo "{$row['username']}"?></td>
                        <td class="text-center">
                            <a href="../common/handle/DeleteTouch.php?id=<?php echo $row['id'];?>">删除</a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>
