<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 11:40
 */

require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Advert.php");

session_start();
sessionIsEmptyByParamThenGotoPage('adminId','../login.php');
?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>查看用户</title>
    <link href="../../public/css/bootstrap.css" rel="stylesheet">
</head>

<body style="margin-top: 10px;">
<div class="container-fluid" id="main-body">
    <div class="row" id="main-body">
        <table class="table table-bordered text-center" style="overflow: scroll">
            <tr>
                <td>编号</td>
                <td>图片</td>
                <td>轮播</td>
                <td>标题</td>
                <td>内容</td>
                <td>链接</td>
                <td>操作</td>
            </tr>
            <?php
            $advert = new Advert($pdo);
            $advertRows = $advert->getAllAdvert();
            if(!$advertRows == null)
            {
                foreach ($advertRows as $row)
                {
                    ?>
                    <tr>
                        <td><?php echo "{$row['id']}"; ?></td>
                        <td><img src="../../public/img/AdvertImg/<?php echo "{$row['img']}"; ?>" style="width: 150px;"></td>
                        <td>
                            <?php
                            if ($row['isUsing'] == 0)
                            {
                                echo "否";
                            }
                            else
                            {
                                echo "是";
                            }
                            ?>
                        </td>
                        <td><?php echo "{$row['advTitle']}";?></td>
                        <td><?php echo "{$row['advContent']}";?></td>
                        <td><a href=""><?php echo "{$row['shop_id']}"; ?></a></td>
                        <td>
                            <a class="pull-left" href="modifyAdvert.php?id=<?php echo $row['id']; ?>">修改</a>
                            <a class="pull-right" href="../common/handle/DeleteAdvert.php?id=<?php echo $row['id']; ?>">删除</a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>