<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 21:29
 */
require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Brand.php");
require_once ("../../public/common/classes/GoodsClass.php");

session_start();

sessionIsEmptyByParamThenGotoPage('adminId','../login.php');
getIsEmptyByParamThenReturn('id');

$brandId = $_GET['id'];
$brand = new Brand($pdo);
$brandRow = $brand->getBrandById(intval($brandId));

?>

<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>修改品牌</title>

    <link href="../../public/css/bootstrap.css" rel="stylesheet">
    <link href="../css/View-addUser.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid" id="main-body">
    <div class="row text-center" id="main-body">
        <div class="panel panel-default col-md-3 col-lg-3 " id="add-user-panel">
            <!-- Default panel contents -->
            <div class="panel-heading">修改品牌</div>
            <div class="panel-body">
                <form class="form-horizontal" action="../common/handle/ModifyBrand.php" method="post">
                    <input type="hidden" name="id" value=
                    "<?php echo "{$brandRow['id']}";?>"
                    >
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="input-username" placeholder="品牌名称" name="brandName" value=
                            "<?php echo "{$brandRow['name']}"; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="input-class">分类名称</label>
                            <select name="class" id="input-class" class="form-control">
                                <?php
                                $goodsClass = new GoodsClass($pdo);
                                $goodsClassRows = $goodsClass->getAllGoodsClass();
                                foreach ($goodsClassRows as $row)
                                {
                                    ?>
                                    <option value=
                                    "<?php echo "{$row['id']}";?>"<?php
                                    if (strval($brandRow['class_id']) == strval($row['id']))
                                    echo "selected";
                                    ?>><?php
                                        echo "{$row['name']}"
                                        ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class=" col-lg-12  col-md-12">
                            <button type="submit" class="btn btn-default">确认修改</button>
                            <a class="btn btn-default" href="viewClass.php">取消操作</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>
