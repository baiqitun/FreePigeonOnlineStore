<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/10
 * Time: 18:36
 */

require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Goods.php");
require_once ("../../public/common/classes/Comment.php");
require_once ("../../public/common/classes/User.php");

session_start();
sessionIsEmptyByParamThenGotoPage('adminId','../login.php');
?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>查看订单状态</title>
    <link href="../../public/css/bootstrap.css" rel="stylesheet">
</head>

<body style="margin-top: 10px;">
<div class="container-fluid" id="main-body">
    <div class="row" id="main-body">
        <table class="table table-bordered text-center" style="overflow: scroll">
            <tr>
                <td>评论编号</td>
                <td>评论用户</td>
                <td>评论内容</td>
                <td>评论商品</td>
                <td>评论时间</td>
                <td>操作</td>
            </tr>
            <?php
            $comment = new Comment($pdo);
            $commentRows = $comment->getAllComment();
            if ($commentRows != null)
            {
                foreach ($commentRows as $row)
                {
                    ?>
                    <tr>
                        <td>
                            <?php echo "{$row['id']}";?>
                        </td>
                        <td>
                            <?php
                            $user = new User($pdo);
                            $userRow = $user->getUserById($row['id']);
                            echo $userRow['username'];
                            ?>
                        </td>
                        <td>
                            <?php
                            echo "{$row['content']}";
                            ?>
                        </td>
                        <td>
                            <?php
                            $goods = new Goods($pdo);
                            $goodsRow = $goods->getGoodsById($row['shop_id']);
                            echo $goodsRow['name'];
                            ?>
                        </td>
                        <td>
                            <?php
                            echo "{$row['time']}";
                            ?>
                        </td>
                        <td>
                            <a href="../common/handle/DeleteComment.php?id=<?php echo $row['id'];?>">删除</a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>
