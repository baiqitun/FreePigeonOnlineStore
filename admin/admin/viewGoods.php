<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/9
 * Time: 8:13
 */

require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/Goods.php");
require_once ("../../public/common/classes/Brand.php");

session_start();
sessionIsEmptyByParamThenGotoPage('adminId','../login.php');
?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>查看分类</title>
    <link href="../../public/css/bootstrap.css" rel="stylesheet">
</head>

<body style="margin-top: 10px;">
<div class="container-fluid" id="main-body">
    <div class="row" id="main-body">
        <table class="table table-bordered text-center" style="overflow: scroll">
            <tr>
                <td>编号</td>
                <td>名称</td>
                <td>图片</td>
                <td>价格</td>
                <td>品牌</td>
                <td>操作</td>
            </tr>
            <?php
            $goods = new Goods($pdo);
            $goodsRows = $goods->getAllGoods();

            $brand = new Brand($pdo);
            if ($goodsRows != null)
            {
                foreach ($goodsRows as $row)
                {
                    $brandRow = $brand->getBrandById($row['brand_id']);
                    ?>
                    <tr>
                        <td><?php echo "{$row['id']}";?></td>
                        <td><?php echo "{$row['name']}";?></td>
                        <td><img src="../../public/img/GoodsImg/small/<?php echo "{$row['img']}"?>" alt=""></td>
                        <td><?php echo "{$row['price']}"?></td>
                        <td><?php echo "{$brandRow['name']}"?></td>
                        <td>
                            <a class="pull-left" href="../admin/modifyGoods.php?id=<?php echo $row['id'];?>">修改</a>
                            <a class="pull-right" href="../common/handle/DeleteGoods.php?id=<?php echo $row['id'];?>">删除</a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>
