<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 20:28
 */

require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/GoodsClass.php");
require_once ("../../public/common/classes/Brand.php");
session_start();
sessionIsEmptyByParamThenGotoPage('adminId','../login.php');

?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>查看品牌</title>
    <link href="../../public/css/bootstrap.css" rel="stylesheet">
</head>

<body style="margin-top: 10px;">
<div class="container-fluid" id="main-body">
    <div class="row" id="main-body">
        <table class="table table-bordered text-center" style="overflow: scroll">
            <tr>
                <td>编号</td>
                <td>品牌名称</td>
                <td>所属分类</td>
                <td>操作</td>
            </tr>
            <?php
            //多表联合查询
            $sql = "SELECT brand.id,brand.name as brandName,class.name as className FROM brand,class WHERE brand.class_id = class.id ORDER BY brand.id";
            $rowCountSql = "SELECT COUNT(*) FROM brand,class WHERE brand.class_id = class.id";

            $stmtRowCount = $pdo->prepare($rowCountSql);
            $stmt = $pdo->prepare($sql);

            $brandWitClassRows = null;
            if($stmtRowCount->execute())
            {
                if($stmtRowCount->fetchColumn()>0)
                {
                    if($stmt->execute())
                    {
                        $brandWitClassRows = $stmt->fetchAll();
                    }
                }
            }
            if ($brandWitClassRows != null)
            {
                foreach ($brandWitClassRows as $row)
                {
                    ?>
                    <tr>
                        <td><?php echo "{$row['id']}";?></td>
                        <td><?php echo "{$row['brandName']}";?></td>
                        <td><?php echo "{$row['className']}"?></td>
                        <td>
                            <a class="pull-left" href="modifyBrand.php?id=<?php echo $row['id'];?>">修改</a>
                            <a class="pull-right" href="../common/handle/DeleteBrand.php?id=<?php echo $row['id'];?>">删除</a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>