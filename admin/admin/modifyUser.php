<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/3
 * Time: 8:53
 */

require_once ("../../public/common/DbConfig.php");
require_once ("../../public/common/db_connect.php");
require_once ("../../public/common/public_include.php");
require_once ("../../public/common/classes/User.php");

session_start();

sessionIsEmptyByParamThenGotoPage('adminId','../login.php');
getIsEmptyByParamThenReturn('id');

$userId = $_GET['id'];
$user = new User($pdo);
$userRow = $user->getUserById(intval($userId));

?>

<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>修改用户</title>

    <link href="../../public/css/bootstrap.css" rel="stylesheet">
    <link href="../css/View-addUser.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid" id="main-body">
    <div class="row text-center" id="main-body">
        <div class="panel panel-default col-md-3 col-lg-3 " id="add-user-panel">
            <!-- Default panel contents -->
            <div class="panel-heading">修改用户</div>
            <div class="panel-body">
                <form class="form-horizontal" action="../common/handle/ModifyUser.php" method="post">
                    <input type="hidden" name="id" value=
                    "<?php echo "{$userRow['id']}";?>"
                    >
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="input-username" placeholder="用户名" name="username" value=
                            "<?php echo "{$userRow['username']}";?>"
                            >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12" >
                            <input type="password" class="form-control" id="input-password" placeholder="密码" name="password" value=
                            "<?php echo "{$userRow['password']}";?>"
                            >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12" >
                            <input type="password" class="form-control" id="input-affirm-password" placeholder="确认密码" name="confirmPassword" value=
                            "<?php echo "{$userRow['password']}";?>"
                            >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class=" col-lg-12  col-md-12">
                            <button type="submit" class="btn btn-default">确认修改</button>
                            <a class="btn btn-default" href="viewUser.php">取消操作</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="../../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../../public/js/bootstrap.js"></script>
</body>

</html>




