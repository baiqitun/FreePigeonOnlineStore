<?php
?>

<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>登录商城后台管理中心</title>

    <link href="../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
</head>

<body>
<form class="login-form" action="common/handle/AdminLogin.php" method="post">
    <h3 class="text-center">管理员登录</h3>
    <div class="tip text-left"></div>
    <input type="text" name="username" id="input-username" placeholder="用户名">
    <br>
    <input type="password" name="password" id="input-password" placeholder="密码">
    <br>
    <input class="input-verificationCode" type="text" name="verificationCode" id="input-verificationCode" placeholder="验证码">
<!--    <img class="verificationCodeImg" src="" alt="">-->
    <br>
    <div class="form-group">
        <div class="text-center">
            <button type="submit" class="btn">登录</button>
        </div>
    </div>
</form>
<script src="../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../public/js/bootstrap.min.js"></script>
</body>

</html>
