<?php
/**
 * Created by PhpStorm.
 * User: LightCloud
 * Date: 2018/6/2
 * Time: 9:45
 */
require_once ("../public/common/DbConfig.php");
require_once ("../public/common/db_connect.php");
require_once ("../public/common/public_include.php");
require_once ("../public/common/classes/User.php");

session_start();
sessionIsEmptyByParamThenGotoPage('adminId','login.php');
?>

<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>网上商城系统后台管理中心</title>

    <link href="../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 main-title text-center">
            <h1>飞鸽网上商城后台管理系统</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <ul class="nav nav-pills navbar-right">
                <li class="navbar-text">尊敬的超级管理员，欢迎您！</li>
                <li role="presentation" class="active">
                    <a href="../home/index.php">前台页面</a>
                </li>
                <li role="presentation">
                    <a href="common/handle/AdminLogout.php?logout=yes">注销</a>
                </li>
                <li role="presentation">
                    <a href="#">虚位以待</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-2">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="user">
                    <div class="user-info text-center">
                        <img src="../public/img/default-user.png">
                </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                用户管理
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <a href="admin/viewUser.php" target="framePage">查看用户</a>
                            <a href="admin/addUser.php" target="framePage">添加用户</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                               aria-controls="collapseTwo">
                               分类管理
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <a href="admin/viewClass.php" target="framePage">查看分类</a>
                            <a href="admin/addClass.php" target="framePage">添加分类</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                               aria-controls="collapseThree">
                                品牌管理
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <a href="admin/viewBrand.php" target="framePage">查看品牌</a>
                            <a href="admin/addBrand.php" target="framePage">添加品牌</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false"
                               aria-controls="collapseFour">
                                商品管理
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                        <div class="panel-body">
                            <a href="admin/viewGoods.php" target="framePage">查看商品</a>
                            <a href="admin/addGoods.php" target="framePage">添加商品</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false"
                               aria-controls="collapseFive">
                                评论管理
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                        <div class="panel-body">
                            <a href="admin/viewComment.php" target="framePage">查看评论</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSix">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false"
                               aria-controls="collapseSix">
                                订单状态
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                        <div class="panel-body">
                            <a href="admin/viewStatus.php" target="framePage">查看订单状态</a>
                            <a href="admin/addStatus.php" target="framePage">添加订单状态</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSeven">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false"
                               aria-controls="collapseSeven">
                                订单管理
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                        <div class="panel-body">
                            <a href="admin/viewOrderInfo.php" target="framePage">查看订单</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingEight">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false"
                               aria-controls="collapseEight">
                                联系方式
                            </a>
                        </h4>
                    </div>
                    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                        <div class="panel-body">
                            <a href="admin/viewTouch.php" target="framePage">查看联系方式</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingNight">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNight" aria-expanded="false"
                               aria-controls="collapseNight">
                                广告管理
                            </a>
                        </h4>
                    </div>
                    <div id="collapseNight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNight">
                        <div class="panel-body">
                            <a href="admin/viewAdvert.php" target="framePage">查看广告</a>
                            <a href="admin/addAdvert.php" target="framePage">添加广告</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <iframe class="col-lg-10 col-md-10 col-sm-12 col-xs-12" src="" frameborder="0" style="height: 700px;" name="framePage">

        </iframe>
    </div>
</div>
<script src="../public/js/jquery-1.12.4/jquery-1.12.4.js"></script>
<script src="../public/js/bootstrap.min.js"></script>
<script src="js/index.js"></script>
</body>

</html>
